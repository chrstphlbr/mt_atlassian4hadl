package net.laaber.mt.hadl.atlassian.data.bitbucket;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by Christoph on 23.10.15.
 */
public class Repository {
    public Repository() {}

    @Key
    private String scm;
    @Key("has_wiki")
    private boolean hasWiki;
    @Key
    private String description;
    @Key
    private GenericJson links;
    @Key("fork_policy")
    private String forkPolicy;
    @Key
    private String name;
    @Key
    private String language;
    @Key("created_on")
    private String createdOn;
    @Key("full_name")
    private String fullName;
    @Key("has_issues")
    private boolean hasIssues;
    @Key
    private User owner;
    @Key("updated_on")
    private String updatedOn;
    @Key
    private long size;
    @Key("is_private")
    private boolean isPrivate;
    @Key
    private String uuid;

    public String getScm() {
        return scm;
    }

    public void setScm(final String scm) {
        this.scm = scm;
    }

    public boolean isHasWiki() {
        return hasWiki;
    }

    public void setHasWiki(final boolean hasWiki) {
        this.hasWiki = hasWiki;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public GenericJson getLinks() {
        return links;
    }

    public void setLinks(final GenericJson links) {
        this.links = links;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public String getForkPolicy() {
        return forkPolicy;
    }

    public void setForkPolicy(final String forkPolicy) {
        this.forkPolicy = forkPolicy;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    public void setCreatedOn(final String createdOn) {
        this.createdOn = createdOn;
    }

    public void setUpdatedOn(final String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public boolean isHasIssues() {
        return hasIssues;
    }

    public void setHasIssues(final boolean hasIssues) {
        this.hasIssues = hasIssues;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(final User owner) {
        this.owner = owner;
    }

    public long getSize() {
        return size;
    }

    public void setSize(final long size) {
        this.size = size;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(final boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "Repository{" +
                "scm='" + scm + '\'' +
                ", hasWiki=" + hasWiki +
                ", description='" + description + '\'' +
                ", links=" + links +
                ", forkPolicy='" + forkPolicy + '\'' +
                ", name='" + name + '\'' +
                ", language='" + language + '\'' +
                ", createdOn=" + createdOn +
                ", fullName='" + fullName + '\'' +
                ", hasIssues=" + hasIssues +
                ", owner=" + owner +
                ", updatedOn=" + updatedOn +
                ", size=" + size +
                ", isPrivate=" + isPrivate +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
