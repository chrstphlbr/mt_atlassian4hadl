package net.laaber.mt.hadl.atlassian.rest;

import fj.data.Option;
import net.laaber.mt.hadl.atlassian.data.hipChat.Chat;
import net.laaber.mt.hadl.atlassian.data.hipChat.Privacy;
import net.laaber.mt.hadl.atlassian.data.hipChat.User;

import java.util.List;

/**
 * Created by Christoph on 23.10.15.
 */
public interface HipChatClient {
    // returns the unique id if successful
    Option<String> createChat(String name, String topic, Privacy privacy, User owner, boolean guestAccess);
    default Option<String> createChat(String name) {
        return createChat(name, null, Privacy.PRIVATE, null, false);
    }
    boolean deleteChat(String idOrName);
    default boolean deleteChat(long id) {
        return deleteChat(String.valueOf(id));
    }
    Option<Chat> chat(String idOrName);
    default Option<Chat> chat(long id) {
        return chat(String.valueOf(id));
    }
    Option<List<User>> usersInChat(String roomIdOrName);
    default Option<List<User>> usersInChat(long id) {
        return usersInChat(String.valueOf(id));
    }
    boolean inviteUserToChat(String userIdOrEmail, String roomIdOrName);
    default boolean inviteUserToChat(long userId, long roomId) {
        return inviteUserToChat(String.valueOf(userId), String.valueOf(roomId));
    }
    default boolean inviteUserToChat(String userIdOrEmail, long roomId) {
        return inviteUserToChat(userIdOrEmail, String.valueOf(roomId));
    }
    default boolean inviteUserToChat(long userId, String roomIdOrName) {
        return inviteUserToChat(String.valueOf(userId), roomIdOrName);
    }
    boolean removeUserFromChat(String userIdOrEmail, String roomIdOrName);
    default boolean removeUserFromChat(long userId, String roomIdOrName) {
        return removeUserFromChat(String.valueOf(userId), roomIdOrName);
    }
    default boolean removeUserFromChat(String userIdOrEmail, long roomId) {
        return removeUserFromChat(userIdOrEmail, String.valueOf(roomId));
    }
    default boolean removeUserFromChat(long userId, long roomId) {
        return removeUserFromChat(String.valueOf(userId), String.valueOf((roomId)));
    }
    Option<User> user(String idOrEmail);
    default Option<User> user(long id) {
        return user(String.valueOf(id));
    }
}
