package net.laaber.mt.hadl.atlassian.surrogate;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.OperationalStateTransitionControl;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.Util;
import net.laaber.mt.hadl.atlassian.data.hipChat.Chat;
import net.laaber.mt.hadl.atlassian.data.hipChat.User;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TAtlassianResourceDescriptor;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.THipChatResourceDescriptor;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import net.laaber.mt.hadl.atlassian.rest.HipChatClient;
import rx.subjects.BehaviorSubject;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 29.10.15.
 */
public class ChatSurrogate extends AbstractAsyncObjectSurrogate {

    private THipChatResourceDescriptor rd;
    private Chat chat;
    private HipChatClient hcClient;
    private List<Map.Entry<User, TOperationalCollabLink>> invites = new ArrayList<>();
    private List<Map.Entry<User, TOperationalCollabLink>> removeInvites = new ArrayList<>();

    public ChatSurrogate() {
    }

    @Override
    public void asyncBegin(final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // invites
        for (Map.Entry<User, TOperationalCollabLink> i : invites) {
            boolean success = hcClient.inviteUserToChat(i.getKey().getEmail(), rd.getHipChatId());
            TOperationalCollabLink l = i.getValue();
            if (success) {
                l.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.DESCRIBED_EXISTING));
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS, i.getValue()));
            } else {
                l.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_EXISTING_DENIED));
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_FAILED, new SurrogateException("Could not add user to chat", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), i.getValue()));
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        invites = new ArrayList<>();

        for (Map.Entry<User, TOperationalCollabLink> i : removeInvites) {
            boolean success = hcClient.removeUserFromChat(i.getKey().getEmail(), rd.getHipChatId());
            TOperationalCollabLink l = i.getValue();
            if (success) {
                l.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_NONEXISTING, TOperationalState.DESCRIBED_NONEXISTING));
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS, i.getValue()));
            } else {
                l.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_NONEXISTING, TOperationalState.PRESCRIBED_NONEXISTING_DENIED));
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_FAILED, new SurrogateException("Could not remove user from chat", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), i.getValue()));
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        removeInvites = new ArrayList<>();

        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncStop(final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        Option<Chat> optChat = hcClient.chat(rd.getHipChatId());
        if (optChat.isNone()) {
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_FAILED, this.oc));
        } else {
            chat = optChat.some();
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, this.oc));
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncRelease(final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.DESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
        boolean success = hcClient.deleteChat(chat.getId());
        if (!success) {
            this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_NONEXISTING, TOperationalState.PRESCRIBED_NONEXISTING_DENIED));
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_FAILED));
        } else {
            this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_NONEXISTING, TOperationalState.DESCRIBED_NONEXISTING));
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS));
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalObject tOperationalObject, final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        Option<THipChatResourceDescriptor> optRd = Util.firstResorurceDescriptorOfType(THipChatResourceDescriptor.class, tOperationalObject.getResourceDescriptor());
        if (optRd.isNone()) {
            this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            behaviorSubject.onError(new SurrogateException("No THipChatResourceDescriptor found", SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR));
            return;
        }
        rd = optRd.some();

        try {
            hcClient = AtlassianClientFactory.instance().hipChatClient();
        } catch (IOException e) {
            e.printStackTrace();
            this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            behaviorSubject.onError(new SurrogateException("Could not load credentials", SurrogateErrorType.INCORRECT_CONFIGURATION));
            return;
        }

        Option<Chat> optChat = hcClient.chat(rd.getHipChatId());
        if (optChat.isNone()) {
            // no chat exists, create one
            Option<String> id = hcClient.createChat(rd.getHipChatId());
            if (id.isNone()) {
                this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_EXISTING_DENIED));
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_FAILED));
                behaviorSubject.onCompleted();
                return;
            }
            chat = new Chat();
            chat.setId(Long.valueOf(id.some()));
            chat.setName(rd.getHipChatId());
            chat.setParticipants(new ArrayList<>());
        } else {
            chat  = optChat.some();
        }

        this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.DESCRIBED_EXISTING));
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, this.oc));
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncLinkTo(final TAction tAction, final TOperationalConnector tOperationalConnector, final TAction tAction1, final BehaviorSubject<SurrogateEvent> behaviorSubject, final TOperationalCollabLink tOperationalCollabLink) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // no connector to link to
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_FAILED, new SurrogateException("ChatSurrogate does not support linking of connector", SurrogateErrorType.UNSUPPORTED_HADLTYPE), tOperationalCollabLink));
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncLinkTo(final TAction ownAction, final TOperationalComponent oppositeComp, final TAction oppositeAction, final BehaviorSubject<SurrogateEvent> behaviorSubject, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // do not check link type as there is only one
        Option<TAtlassianResourceDescriptor> userRd = Util.firstResorurceDescriptorOfType(TAtlassianResourceDescriptor.class, oppositeComp.getResourceDescriptor());
        if (userRd.isNone()) {
            behaviorSubject.onError(new SurrogateException("No RD for user", SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR));
            return;
        }
        User u = new User();
        u.setEmail(userRd.some().getHipChat().getHipChatId());
        invites.add(new AbstractMap.SimpleEntry<>(u, link));
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncDisconnectFrom(final TAction ownAction, final TOperationalComponent oppositeComp, final BehaviorSubject<SurrogateEvent> behaviorSubject, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        link.setState(OperationalStateTransitionControl.doTransition(TOperationalState.DESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
        Option<TAtlassianResourceDescriptor> userRd = Util.firstResorurceDescriptorOfType(TAtlassianResourceDescriptor.class, oppositeComp.getResourceDescriptor());
        if (userRd.isNone()) {
            behaviorSubject.onError(new SurrogateException("No RD for user", SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR));
            return;
        }
        String email = userRd.some().getHipChat().getHipChatId();
        User u = new User();
        u.setEmail(email);
        removeInvites.add(new AbstractMap.SimpleEntry<>(u, link));
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncDisconnectFrom(final TAction tAction, final TOperationalConnector tOperationalConnector, final BehaviorSubject<SurrogateEvent> behaviorSubject, final TOperationalCollabLink tOperationalCollabLink) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // no connector to unlink from
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_FAILED, new SurrogateException("ChatSurrogate does not support unlinking of connector", SurrogateErrorType.UNSUPPORTED_HADLTYPE), tOperationalCollabLink));
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncRelating(final TOperationalObject tOperationalObject, final TObjectRef tObjectRef, final boolean b, final boolean b1, final BehaviorSubject<SurrogateEvent> behaviorSubject, final TOperationalObjectRef tOperationalObjectRef) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // no relations to handle
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_FAILED, new SurrogateException("ChatSurrogate does not support references", SurrogateErrorType.UNSUPPORTED_HADLTYPE), tOperationalObjectRef));
        behaviorSubject.onCompleted();
    }
}
