package net.laaber.mt.hadl.atlassian.data.hipChat;

import com.google.api.client.util.Key;

/**
 * Created by Christoph on 28.10.15.
 */
public class Plan {
    public Plan() {}

    @Key
    private String type;
    @Key
    private long id;

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Plan plan = (Plan) o;

        return id == plan.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Plan{" +
                "type='" + type + '\'' +
                ", id=" + id +
                '}';
    }
}
