package net.laaber.mt.hadl.atlassian.resourceDescriptor;

/**
 * Created by Christoph on 03.11.15.
 */
public final class AtlassianResourceDescriptorFactory {
    private static final String atlassianPrefix = "at";
    private static final String bitbucketPrefix = "bb";
    private static final String hipChatPrefix = "hc";

    private AtlassianResourceDescriptorFactory() {}

    public static TBitbucketResourceDescriptor bitbucket(String name, String bitbucketId) {
        TBitbucketResourceDescriptor rd = new TBitbucketResourceDescriptor();
        rd.setId(bitbucketPrefix + ":" + bitbucketId);
        rd.setName(name);
        rd.setBitbucketId(bitbucketId);
        return rd;
    }

    public static THipChatResourceDescriptor hipChat(String name, String hipChatId) {
        THipChatResourceDescriptor rd = new THipChatResourceDescriptor();
        rd.setId(hipChatPrefix + ":" + hipChatId);
        rd.setName(name);
        rd.setHipChatId(hipChatId);
        return rd;
    }

    public static TAtlassianResourceDescriptor atlassian(String name, String hipChatId, String bitucketId) {
        TAtlassianResourceDescriptor rd = new TAtlassianResourceDescriptor();
        rd.setId(atlassianPrefix + ":" + hipChatId + ":" + bitucketId);
        rd.setName(name);
        rd.setBitbucket(bitbucket(bitbucketPrefix + name, bitucketId));
        rd.setHipChat(hipChat(hipChatPrefix + name, hipChatId));
        return rd;
    }
}
