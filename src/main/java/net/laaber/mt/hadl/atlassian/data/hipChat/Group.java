package net.laaber.mt.hadl.atlassian.data.hipChat;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

/**
 * Created by Christoph on 28.10.15.
 */
public class Group {
    public Group() {}

    @Key
    private long id;
    @Key
    private String name;
    @Key
    private GenericJson links;
    @Key
    private String domain;
    @Key
    private GenericJson statistics;
    @Key("avatar_url")
    private String avatarUrl;
    @Key
    private Plan plan;
    @Key
    private User owner;
    @Key
    private String subdomain;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public GenericJson getLinks() {
        return links;
    }

    public void setLinks(final GenericJson links) {
        this.links = links;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(final String domain) {
        this.domain = domain;
    }

    public GenericJson getStatistics() {
        return statistics;
    }

    public void setStatistics(final GenericJson statistics) {
        this.statistics = statistics;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(final String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(final Plan plan) {
        this.plan = plan;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(final User owner) {
        this.owner = owner;
    }

    public String getSubdomain() {
        return subdomain;
    }

    public void setSubdomain(final String subdomain) {
        this.subdomain = subdomain;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Group group = (Group) o;

        return id == group.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", links=" + links +
                ", domain='" + domain + '\'' +
                ", statistics=" + statistics +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", plan=" + plan +
                ", owner=" + owner +
                ", subdomain='" + subdomain + '\'' +
                '}';
    }
}
