package net.laaber.mt.hadl.atlassian.rest;

import fj.data.Option;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Repository;
import net.laaber.mt.hadl.atlassian.data.bitbucket.User;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;

import java.util.List;

/**
 * Created by Christoph on 22.10.15.
 */
public interface BitbucketClient {
    Option<User> user(String userId);
    Option<List<Repository>> repositories(String owner);
    Option<Repository> repository(String owner, String repo);
    Option<List<User>> teamMembers(String teamName);
    Option<Wiki> wiki(String owner, String repo, String pageTitle);
    boolean createWiki(String owner, String repo,  String pageTitle, String content);
    boolean updateWiki(String owner, String repo,  String pageTitle, String path, String content);
}
