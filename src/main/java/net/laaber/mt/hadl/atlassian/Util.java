package net.laaber.mt.hadl.atlassian;

import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;
import net.laaber.mt.hadl.atlassian.rest.BitbucketClient;
import net.laaber.mt.hadl.atlassian.rest.HipChatClient;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Christoph on 28.10.15.
 */
public final class Util {
    private Util() {}

    public static String url(String... parts) {
        if (parts.length == 0) {
            return "";
        }
        StringBuilder url = new StringBuilder();
        url.append(parts[0]);
        for (int i = 1; i < parts.length; i++) {
            url.append("/");
            url.append(parts[i]);
        }
        return url.toString();
    }

    public static <T extends TResourceDescriptor> Option<T> firstResorurceDescriptorOfType(Class<T> rdType, List<TResourceDescriptor> rds) {
        for (TResourceDescriptor rd : rds) {
            if (rdType.isAssignableFrom(rd.getClass())) {
                return Option.some(rdType.cast(rd));
            }
        }
        return Option.none();
    }

    public static boolean sendGmailMail(String username, String password, String from, String to, String subject, String message) {
        Properties p = new Properties();
        p.put("mail.smtp.auth", "true");
        p.put("mail.smtp.starttls.enable", "true");
        p.put("mail.smtp.host", "smtp.gmail.com");
        p.put("mail.smtp.port", "587");
        Session s = Session.getInstance(p, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });
        Message m = new MimeMessage(s);
        try {
            m.setFrom(new InternetAddress(from));
            m.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            m.setSubject(subject);
            m.setText(message);
            Transport.send(m);
            return true;
        } catch (MessagingException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static Option<Map.Entry<String, String>> previousNext(BitbucketClient client, String account, String repository, String wikiName) {
        final Option<Wiki> wikiRes = client.wiki(account, repository, wikiName);
        if (wikiRes.isNone()) {
            return Option.none();
        }
        Wiki wiki = wikiRes.some();
        return previousNext(wiki.getData());
    }

    public static Option<Map.Entry<String, String>> previousNext(String wikiContent) {
        String[] lines = wikiContent.split("\n");
        if (lines.length < 1) {
            return Option.none();
        }

        // get first line and check for prev, next
        String firstLine = lines[0];
        if (firstLine.startsWith("!!")) {
            firstLine = firstLine.substring(2);
            String[] prevNext = firstLine.split(",");
            if (prevNext.length == 2) {
                return Option.some(new AbstractMap.SimpleImmutableEntry<>(prevNext[0], prevNext[1]));
            } else if (prevNext.length == 1) {
                return Option.some(new AbstractMap.SimpleImmutableEntry<>(prevNext[0], null));
            }
        }
        return Option.none();
    }
}
