//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.11.03 um 11:57:26 AM CET 
//


package net.laaber.mt.hadl.atlassian.resourceDescriptor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;


/**
 * <p>Java-Klasse für tHipChatResourceDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="tHipChatResourceDescriptor">
 *   &lt;complexContent>
 *     &lt;extension base="{http://at.ac.tuwien.dsg/hADL/hADLruntime}tResourceDescriptor">
 *       &lt;attribute name="hipChatId" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tHipChatResourceDescriptor")
public class THipChatResourceDescriptor
    extends TResourceDescriptor
{

    @XmlAttribute(name = "hipChatId")
    protected String hipChatId;

    /**
     * Ruft den Wert der hipChatId-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHipChatId() {
        return hipChatId;
    }

    /**
     * Legt den Wert der hipChatId-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHipChatId(String value) {
        this.hipChatId = value;
    }

}
