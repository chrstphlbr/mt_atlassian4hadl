package net.laaber.mt.hadl.atlassian.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalConnector;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.HadlConstants;
import net.laaber.mt.hadl.atlassian.Util;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TBitbucketResourceDescriptor;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import net.laaber.mt.hadl.atlassian.rest.BitbucketClient;
import rx.subjects.BehaviorSubject;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Map;

/**
 * Created by Christoph on 30.10.15.
 */
public class WikiSensor extends AbstractAsyncSensor {
    private static final String propTeamname = "bb.teamname";
    private static final String propRepository = "bb.repository";

    private final ModelTypesUtil mtu;

    private BitbucketClient client;
    private TBitbucketResourceDescriptor rd;
    private Wiki wiki;
    private String teamname;
    private String repository;

    WikiSensor(ModelTypesUtil mtu) {
        this.mtu = mtu;
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalComponent tOperationalComponent, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onError(new IllegalStateException("WikiSensor does not support asyncAcquire of TOperationalComponent"));
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalConnector tOperationalConnector, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onError(new IllegalStateException("WikiSensor does not support asyncAcquire of TOperationalConnector"));
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalObject tOperationalObject, final BehaviorSubject<SensorEvent> behaviorSubject) {
        behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_INPROGRESS, this));
        final Option<TBitbucketResourceDescriptor> optRd = Util.firstResorurceDescriptorOfType(TBitbucketResourceDescriptor.class, tOperationalObject.getResourceDescriptor());
        if (optRd.isNone()) {
            behaviorSubject.onError(new IllegalStateException("No RD for Wiki"));
            return;
        }
        rd = optRd.some();
        try {
            AtlassianClientFactory f = AtlassianClientFactory.instance();
            client = f.bitbucketClient();
            teamname = f.properties().getProperty(propTeamname);
            repository = f.properties().getProperty(propRepository);
            final Option<Wiki> wiki = client.wiki(teamname, repository, rd.getBitbucketId());
            if (wiki.isNone()) {
                behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_FAILED, this));
            } else {
                this.wiki = wiki.some();
                behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_SUCCESS, this));
            }
        } catch (IOException e) {
            e.printStackTrace();
            behaviorSubject.onNext(new SensorEvent(SensorEvent.ESensorStatus.ACQUIRING_FAILED, e, this));
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncLoadOnce(final BehaviorSubject<LoadEvent> behaviorSubject, final SensingScope sensingScope) {
        // only obj refs are sensed
        for (TObjectRef r : sensingScope.getObjectRefs()) {
            switch (r.getId()) {
                case HadlConstants.ObjRef.PrevWiki:
                    handlePrevWiki(r, behaviorSubject, sensingScope);
                    break;
                case HadlConstants.ObjRef.NextWiki:
                    handleNextWiki(r, behaviorSubject, sensingScope);
                    break;
                default:
                    continue;
            }
        }
        behaviorSubject.onCompleted();
    }

    private void handlePrevWiki(final TObjectRef r, final BehaviorSubject<LoadEvent> s, final SensingScope scope) {
        final Option<Map.Entry<String, String>> entries = Util.previousNext(client, teamname, repository, wiki.getPage());
        if (entries.isNone()) {
            s.onCompleted();
            return;
        }
        String prevName = entries.some().getKey();
        if (prevName == null || prevName.isEmpty()) {
            s.onCompleted();
            return;
        }
        loadWiki(prevName, r, s, scope);
    }

    private void handleNextWiki(final TObjectRef r, final BehaviorSubject<LoadEvent> s, final SensingScope scope) {
        final Option<Map.Entry<String, String>> entries = Util.previousNext(client, teamname, repository, wiki.getPage());
        if (entries.isNone()) {
            s.onCompleted();
            return;
        }
        String nextName = entries.some().getValue();
        if (nextName == null || nextName.isEmpty()) {
            s.onCompleted();
            return;
        }
        loadWiki(nextName, r, s, scope);
    }

    private void loadWiki(final String name, final TObjectRef r, final BehaviorSubject<LoadEvent> s, final SensingScope scope) {
        Option<Wiki> w = client.wiki(teamname, repository, name);
        if (w.isSome()) {
            OperativeObjectEvent e = new OperativeObjectEvent((TCollabObject) mtu.getById(HadlConstants.Obj.Wiki), oc, scope);
            e.getLoadedViaRef().add(r);
            e.addDescriptor(AtlassianResourceDescriptorFactory.bitbucket(w.some().getPage(), w.some().getPage()));
            s.onNext(e);
        }
        s.onCompleted();
    }
}
