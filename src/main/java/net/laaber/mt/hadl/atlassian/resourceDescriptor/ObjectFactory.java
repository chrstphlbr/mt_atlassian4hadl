//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.11.03 um 11:57:26 AM CET 
//


package net.laaber.mt.hadl.atlassian.resourceDescriptor;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.laaber.mt.hadl.atlassian.resourceDescriptor package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _BitbucketDescriptor_QNAME = new QName("http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket", "BitbucketDescriptor");
    private final static QName _HipChatDescriptor_QNAME = new QName("http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket", "HipChatDescriptor");
    private final static QName _AtlassianDescriptor_QNAME = new QName("http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket", "AtlassianDescriptor");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.laaber.mt.hadl.atlassian.resourceDescriptor
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link THipChatResourceDescriptor }
     * 
     */
    public THipChatResourceDescriptor createTHipChatResourceDescriptor() {
        return new THipChatResourceDescriptor();
    }

    /**
     * Create an instance of {@link TBitbucketResourceDescriptor }
     * 
     */
    public TBitbucketResourceDescriptor createTBitbucketResourceDescriptor() {
        return new TBitbucketResourceDescriptor();
    }

    /**
     * Create an instance of {@link TAtlassianResourceDescriptor }
     * 
     */
    public TAtlassianResourceDescriptor createTAtlassianResourceDescriptor() {
        return new TAtlassianResourceDescriptor();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TBitbucketResourceDescriptor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket", name = "BitbucketDescriptor")
    public JAXBElement<TBitbucketResourceDescriptor> createBitbucketDescriptor(TBitbucketResourceDescriptor value) {
        return new JAXBElement<TBitbucketResourceDescriptor>(_BitbucketDescriptor_QNAME, TBitbucketResourceDescriptor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link THipChatResourceDescriptor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket", name = "HipChatDescriptor")
    public JAXBElement<THipChatResourceDescriptor> createHipChatDescriptor(THipChatResourceDescriptor value) {
        return new JAXBElement<THipChatResourceDescriptor>(_HipChatDescriptor_QNAME, THipChatResourceDescriptor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TAtlassianResourceDescriptor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket", name = "AtlassianDescriptor")
    public JAXBElement<TAtlassianResourceDescriptor> createAtlassianDescriptor(TAtlassianResourceDescriptor value) {
        return new JAXBElement<TAtlassianResourceDescriptor>(_AtlassianDescriptor_QNAME, TAtlassianResourceDescriptor.class, null, value);
    }

}
