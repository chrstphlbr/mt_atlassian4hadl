package net.laaber.mt.hadl.atlassian.data.hipChat;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

import javax.print.DocFlavor;
import java.util.Date;
import java.util.List;

/**
 * Created by Christoph on 22.10.15.
 */
public class User {
    public User() {}

    @Key
    private String email;
    @Key
    private String created;
    @Key
    private Group group;
    @Key
    private long id;
    @Key("is_deleted")
    private boolean isDeleted;
    @Key("is_group_admin")
    private boolean isGroupAdmin;
    @Key("is_guest")
    private boolean isGuest;
    @Key("last_active")
    private String lastActive;
    @Key
    private GenericJson links;
    @Key("mention_name")
    private String mentionName;
    @Key
    private String name;
    @Key("photo_url")
    private String photoUrl;
    @Key
    private GenericJson presence;
    @Key
    private List<String> roles;
    @Key
    private String timezone;
    @Key
    private String title;
    @Key
    private String version;
    @Key("xmpp_jid")
    private String xmppJid;

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(final String created) {
        this.created = created;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(final Group group) {
        this.group = group;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(final boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public boolean isGroupAdmin() {
        return isGroupAdmin;
    }

    public void setIsGroupAdmin(final boolean isGroupAdmin) {
        this.isGroupAdmin = isGroupAdmin;
    }

    public boolean isGuest() {
        return isGuest;
    }

    public void setIsGuest(final boolean isGuest) {
        this.isGuest = isGuest;
    }

    public String getLastActive() {
        return lastActive;
    }

    public void setLastActive(final String lastActive) {
        this.lastActive = lastActive;
    }

    public GenericJson getLinks() {
        return links;
    }

    public void setLinks(final GenericJson links) {
        this.links = links;
    }

    public String getMentionName() {
        return mentionName;
    }

    public void setMentionName(final String mentionName) {
        this.mentionName = mentionName;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(final String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public GenericJson getPresence() {
        return presence;
    }

    public void setPresence(final GenericJson presence) {
        this.presence = presence;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(final List<String> roles) {
        this.roles = roles;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(final String timezone) {
        this.timezone = timezone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public String getXmppJid() {
        return xmppJid;
    }

    public void setXmppJid(final String xmppJid) {
        this.xmppJid = xmppJid;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final User user = (User) o;

        return id == user.id || email.equals(user.email);

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", created='" + created + '\'' +
                ", group=" + group +
                ", id='" + id + '\'' +
                ", isDeleted=" + isDeleted +
                ", isGroupAdmin=" + isGroupAdmin +
                ", isGuest=" + isGuest +
                ", lastActive='" + lastActive + '\'' +
                ", links=" + links +
                ", mentionName='" + mentionName + '\'' +
                ", name='" + name + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", presence=" + presence +
                ", roles=" + roles +
                ", timezone='" + timezone + '\'' +
                ", title='" + title + '\'' +
                ", version='" + version + '\'' +
                ", xmppJid='" + xmppJid + '\'' +
                '}';
    }
}
