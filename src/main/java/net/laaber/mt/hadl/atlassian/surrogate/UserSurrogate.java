package net.laaber.mt.hadl.atlassian.surrogate;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.OperationalStateTransitionControl;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.Util;
import net.laaber.mt.hadl.atlassian.data.bitbucket.User;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TAtlassianResourceDescriptor;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import net.laaber.mt.hadl.atlassian.rest.BitbucketClient;
import net.laaber.mt.hadl.atlassian.rest.HipChatClient;
import rx.subjects.BehaviorSubject;

import java.io.IOException;

/**
 * Created by Christoph on 28.10.15.
 */
public class UserSurrogate extends AbstractAsyncCollaboratorSurrogate {

    private TAtlassianResourceDescriptor rd;
    private User bitbucketUser;
    private net.laaber.mt.hadl.atlassian.data.hipChat.User hipChatUser;
    private HipChatClient hcClient;
    private BitbucketClient bbClient;

    public UserSurrogate() {
    }

    @Override
    public void asyncBegin(final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // nothing to update already done in
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS));
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncStop(final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // load newest info
        Option<net.laaber.mt.hadl.atlassian.data.hipChat.User> hcUser = hcClient.user(rd.getHipChat().getHipChatId());
        if (hcUser.isNone()) {
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_FAILED, this.oc1));
        } else {
            Option<User> bbUser = bbClient.user(rd.getBitbucket().getBitbucketId());
            if (bbUser.isNone()) {
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_FAILED, this.oc1));
            } else {
                bitbucketUser = bbUser.some();
                hipChatUser = hcUser.some();
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, this.oc1));
            }
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncRelease(final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        // nothing to delete here
        this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.DESCRIBED_EXISTING, TOperationalState.DESCRIBED_NONEXISTING));
        behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS));
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalComponent tOperationalComponent, final BehaviorSubject<SurrogateEvent> behaviorSubject) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            behaviorSubject.onCompleted();
            return;
        }
        Option<TAtlassianResourceDescriptor> optRd = Util.firstResorurceDescriptorOfType(TAtlassianResourceDescriptor.class, tOperationalComponent.getResourceDescriptor());
        if (optRd.isNone()) {
            this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            behaviorSubject.onError(new SurrogateException("No TAtlassianResourceDescriptor found", SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR));
            return;
        }
        rd = optRd.some();

        if (rd.getBitbucket() == null || rd.getHipChat() == null) {
            this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            behaviorSubject.onError(new SurrogateException("TAtlassianResourceDescriptor does not contain either TBitucketResourceDescriptor or THipChatResourceDescriptor", SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR));
            return;
        }

        try {
            hcClient = AtlassianClientFactory.instance().hipChatClient();
            bbClient = AtlassianClientFactory.instance().bitbucketClient();
        } catch (IOException e) {
            e.printStackTrace();
            this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            behaviorSubject.onError(new SurrogateException("Could not load credentials", e, SurrogateErrorType.INCORRECT_CONFIGURATION));
            return;
        }

        Option<net.laaber.mt.hadl.atlassian.data.hipChat.User> hcUser = hcClient.user(rd.getHipChat().getHipChatId());
        if (hcUser.isNone()) {
            this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_EXISTING_DENIED));
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_FAILED));
        } else {
            Option<User> bbUser = bbClient.user(rd.getBitbucket().getBitbucketId());
            if (bbUser.isNone()) {
                this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_EXISTING_DENIED));
                behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_FAILED));
                return;
            }
            bitbucketUser = bbUser.some();
            hipChatUser = hcUser.some();
            this.oc1.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.DESCRIBED_EXISTING));
            behaviorSubject.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, this.oc1));
        }
        behaviorSubject.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope tActivityScope, final TOperationalConnector tOperationalConnector, final BehaviorSubject<SurrogateEvent> s) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_FAILED, new SurrogateException("UserSurrogate does not support acquiring of connectors", SurrogateErrorType.UNSUPPORTED_HADLTYPE), tOperationalConnector));
        s.onCompleted();
    }

    @Override
    public void asyncLinkTo(final TAction localAction, final TOperationalObject oppositeElement, final TAction oppositeAction, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // linking done in ChatSurrogate
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
        s.onCompleted();
    }

    @Override
    public void asyncDisconnectFrom(final TAction localAction, final TOperationalObject oppositeElement, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // unlinking done in ChatSurrogate
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_SUCCESS, link));
        s.onCompleted();
    }

    @Override
    public void asyncRelating(final TOperationalConnector oppositeElement, final TCollabRef relationType, final boolean isRelationOrigin, final boolean doRemove, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabRef ref) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // no relating done
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_FAILED, new SurrogateException("UserSurrogate does not support references", SurrogateErrorType.UNSUPPORTED_HADLTYPE), ref));
        s.onCompleted();
    }

    @Override
    public void asyncRelating(final TOperationalComponent oppositeElement, final TCollabRef relationType, final boolean isRelationOrigin, final boolean doRemove, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabRef ref) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // no unrelating done
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_FAILED, new SurrogateException("UserSurrogate does not support references", SurrogateErrorType.UNSUPPORTED_HADLTYPE), ref));
        s.onCompleted();
    }
}
