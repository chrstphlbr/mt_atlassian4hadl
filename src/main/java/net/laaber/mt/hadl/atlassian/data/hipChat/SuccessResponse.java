package net.laaber.mt.hadl.atlassian.data.hipChat;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

import java.util.List;

/**
 * Created by Christoph on 28.10.15.
 */
public class SuccessResponse {
    public SuccessResponse() {}

    @Key
    private List<GenericJson> items;
    @Key
    private long startIndex;
    @Key
    private long maxResults;
    @Key
    private GenericJson links;

    public List<GenericJson> getItems() {
        return items;
    }

    public void setItems(final List<GenericJson> items) {
        this.items = items;
    }

    public long getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(final long startIndex) {
        this.startIndex = startIndex;
    }

    public long getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(final long maxResults) {
        this.maxResults = maxResults;
    }

    public GenericJson getLinks() {
        return links;
    }

    public void setLinks(final GenericJson links) {
        this.links = links;
    }

    @Override
    public String toString() {
        return "SuccessResponse{" +
                "items=" + items +
                ", startIndex=" + startIndex +
                ", maxResults=" + maxResults +
                ", links=" + links +
                '}';
    }
}
