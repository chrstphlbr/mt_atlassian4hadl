package net.laaber.mt.hadl.atlassian.data.hipChat;

/**
 * Created by Christoph on 28.10.15.
 */
public enum Privacy {
    PUBLIC, PRIVATE;

    public String toString() {
        return this.name().toLowerCase();
    }
}
