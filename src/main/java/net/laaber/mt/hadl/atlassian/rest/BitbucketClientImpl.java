package net.laaber.mt.hadl.atlassian.rest;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.BitbucketConstants;
import net.laaber.mt.hadl.atlassian.Util;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Repository;
import net.laaber.mt.hadl.atlassian.data.bitbucket.SuccessResponse;
import net.laaber.mt.hadl.atlassian.data.bitbucket.User;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static net.laaber.mt.hadl.atlassian.BitbucketConstants.accessTokenUrl;
import static net.laaber.mt.hadl.atlassian.BitbucketConstants.authorizeUrl;

/**
 * Created by Christoph on 23.10.15.
 */
public class BitbucketClientImpl implements BitbucketClient {

    private static final HttpTransport httpTransport = new NetHttpTransport();
    private static final JsonFactory jsonFactory = new JacksonFactory();

    private final String account;
    private final String password;
    private final String apiKey;
    private final String apiSecret;

    public BitbucketClientImpl(final String account, final String password, final String apiKey, String apiSecret) {
        this.account = account;
        this.password = password;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
    }

    private Option<Credential> authorize() {
        try {
            File f = new File("store/bitbucket");
            f.mkdirs();
            AuthorizationCodeFlow flow = new AuthorizationCodeFlow.Builder(BearerToken.authorizationHeaderAccessMethod(),
                    httpTransport,
                    jsonFactory,
                    new GenericUrl(accessTokenUrl),
                    new BasicAuthentication(apiKey, apiSecret),
                    apiKey,
                    authorizeUrl)
                    .setScopes(Arrays.asList(""))
                    .setDataStoreFactory(new FileDataStoreFactory(f))
                    .build();
            Credential c = flow.loadCredential(apiKey);
            if (c == null) {
                LocalServerReceiver r = new LocalServerReceiver.Builder().setHost("127.0.0.1").setPort(8080).build();
                c = new AuthorizationCodeInstalledApp(flow, r).authorize(account);
            }
            return Option.some(c);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    private Option<HttpRequestFactory> oauthRequestFactory() {
        final Option<Credential> c = authorize();
        if (c.isNone()) {
            System.out.println("No credential");
            return Option.none();
        }
        HttpRequestFactory f = httpTransport.createRequestFactory(httpRequest -> {
            c.some().initialize(httpRequest);
            httpRequest.setParser(new JsonObjectParser(jsonFactory));
        });
        return Option.some(f);
    }

    private HttpRequestFactory basicRequestFactory() {
        final BasicAuthentication auth = new BasicAuthentication(account, password);
        HttpRequestFactory f = httpTransport.createRequestFactory(request -> {
            auth.initialize(request);
            request.setParser(new JsonObjectParser(jsonFactory));
        });
        return f;
    }

    @Override
    public Option<User> user(final String userId) {
        Option<HttpRequestFactory> rf = oauthRequestFactory();
        if (rf.isNone()) {
            return Option.none();
        }
        String url = Util.url(BitbucketConstants.APIv2.BaseUrl, BitbucketConstants.APIv2.Users, userId);
        try {
            final HttpRequest req = rf.some().buildGetRequest(new GenericUrl(url));
            User u = req.execute().parseAs(User.class);
            return Option.some(u);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public Option<List<Repository>> repositories(final String owner) {
        final Option<HttpRequestFactory> rf = oauthRequestFactory();
        if (rf.isNone()) {
            return Option.none();
        }
        String url = Util.url(BitbucketConstants.APIv2.BaseUrl, BitbucketConstants.APIv2.Repositories, owner);
        try {
            HttpRequest r = rf.some().buildGetRequest(new GenericUrl(url));
            SuccessResponse resp = r.execute().parseAs(SuccessResponse.class);
            List<Repository> repos = new ArrayList<>();
            jsonFactory.createJsonParser(resp.getValues().toString()).parseArrayAndClose(repos, Repository.class);
            return Option.some(repos);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public Option<Repository> repository(final String owner, final String repo) {
        Option<HttpRequestFactory> rf = oauthRequestFactory();
        if (rf.isNone()) {
            return Option.none();
        }
        String url = Util.url(BitbucketConstants.APIv2.BaseUrl, BitbucketConstants.APIv2.Repositories, owner, repo);
        try {
            HttpRequest r = rf.some().buildGetRequest(new GenericUrl(url));
            Repository repository = r.execute().parseAs(Repository.class);
            return Option.some(repository);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public Option<List<User>> teamMembers(final String teamName) {
        Option<HttpRequestFactory> rf = oauthRequestFactory();
        if (rf.isNone()) {
            return Option.none();
        }
        String url = Util.url(BitbucketConstants.APIv2.BaseUrl, BitbucketConstants.APIv2.Teams, teamName, BitbucketConstants.APIv2.TeamMembers);
        try {
            HttpRequest r = rf.some().buildGetRequest(new GenericUrl(url));
            SuccessResponse result = r.execute().parseAs(SuccessResponse.class);
            List<User> users = new ArrayList<>();
            jsonFactory.createJsonParser(result.getValues().toString()).parseArrayAndClose(users, User.class);
            return Option.some(users);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public Option<Wiki> wiki(final String owner, final String repo, final String pageTitle) {
        HttpRequestFactory rf = basicRequestFactory();
        String url = Util.url(BitbucketConstants.APIv1.BaseUrl, BitbucketConstants.APIv1.Repositories, owner, repo, BitbucketConstants.APIv1.Wiki, pageTitle);
        try {
            HttpRequest r = rf.buildGetRequest(new GenericUrl(url));
            Wiki w = r.execute().parseAs(Wiki.class);
            w.setPage(pageTitle);
            return Option.some(w);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public boolean createWiki(final String owner, final String repo, final String pageTitle, final String content) {
        HttpRequestFactory rf = basicRequestFactory();
        String url = Util.url(BitbucketConstants.APIv1.BaseUrl, BitbucketConstants.APIv1.Repositories, owner, repo, BitbucketConstants.APIv1.Wiki, pageTitle);
        try {
            Map<String, String> data = new HashMap<>(1);
            data.put("data", content);
            data.put("path", pageTitle);
            final HttpRequest req = rf.buildPostRequest(new GenericUrl(url), new UrlEncodedContent(data));
            HttpResponse res = req.execute();
            return res.isSuccessStatusCode();
        } catch (IOException e) {
//            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateWiki(final String owner, final String repo, final String pageTitle, final String path, final String content) {
        HttpRequestFactory rf = basicRequestFactory();
        String url = Util.url(BitbucketConstants.APIv1.BaseUrl, BitbucketConstants.APIv1.Repositories, owner, repo, BitbucketConstants.APIv1.Wiki, pageTitle);
        try {
            Map<String, String> data = new HashMap<>();
            data.put("data", content);
            data.put("path", path);
            final HttpRequest req = rf.buildPutRequest(new GenericUrl(url), new UrlEncodedContent(data));
            return req.execute().isSuccessStatusCode();
        } catch (IOException e) {
//            e.printStackTrace();
            return false;
        }
    }
}
