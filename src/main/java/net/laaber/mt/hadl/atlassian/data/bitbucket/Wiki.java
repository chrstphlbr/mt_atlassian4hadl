package net.laaber.mt.hadl.atlassian.data.bitbucket;

import com.google.api.client.util.Key;

/**
 * Created by Christoph on 22.10.15.
 */
public class Wiki implements Cloneable {
    public Wiki() {}

    // title of the page
    @Key
    private String page;
    // path to the page
    @Key
    private String path;
    @Key
    private String data;
    @Key
    private String markup;
    @Key
    private String rev;

    private String next;

    private String previous;

    public String getPage() {
        return page;
    }

    public void setPage(final String page) {
        this.page = page;
    }

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }

    public String getData() {
        return data;
    }

    public void setData(final String data) {
        this.data = data;
    }

    public String getMarkup() {
        return markup;
    }

    public void setMarkup(final String markup) {
        this.markup = markup;
    }

    public String getRev() {
        return rev;
    }

    public void setRev(final String rev) {
        this.rev = rev;
    }

    public String getNext() {
        return next;
    }

    public void setNext(final String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(final String previous) {
        this.previous = previous;
    }

    @Override
    public String toString() {
        return "Wiki{" +
                "page='" + page + '\'' +
                ", path='" + path + '\'' +
                ", data='" + data + '\'' +
                ", markup='" + markup + '\'' +
                ", rev='" + rev + '\'' +
                ", next='" + next + '\'' +
                ", previous='" + previous + '\'' +
                '}';
    }

    @Override
    public Wiki clone() {
        Wiki w = new Wiki();
        w.page = page;
        w.path = path;
        w.data = data;
        w.markup = markup;
        w.rev = rev;
        w.previous = previous;
        w.next = next;
        return w;
    }
}
