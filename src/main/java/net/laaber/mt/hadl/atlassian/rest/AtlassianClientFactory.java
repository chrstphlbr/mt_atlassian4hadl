package net.laaber.mt.hadl.atlassian.rest;

import net.laaber.mt.hadl.atlassian.BitbucketConstants;
import net.laaber.mt.hadl.atlassian.HipChatConstants;

import java.io.IOException;
import java.util.Properties;

/**
 * Created by Christoph on 28.10.15.
 */
public final class AtlassianClientFactory {

    private volatile static AtlassianClientFactory instance;
    private Properties properties;

    private AtlassianClientFactory() throws IOException {
        properties = new Properties();
        properties.loadFromXML(this.getClass().getClassLoader().getResourceAsStream("atlassianCredentials.xml"));
    }

    public static AtlassianClientFactory instance() throws IOException {
        if (instance == null) {
            synchronized (AtlassianClientFactory.class) {
                if (instance == null) {
                    instance = new AtlassianClientFactory();
                }
            }
        }
        return instance;
    }

    public HipChatClient hipChatClient() {
        return new HipChatClientImpl(properties.getProperty(HipChatConstants.AccessToken));
    }

    public BitbucketClient bitbucketClient() {
        return new BitbucketClientImpl(properties.getProperty(BitbucketConstants.AdminLogin),
                                        properties.getProperty(BitbucketConstants.AdminPassword),
                                        properties.getProperty(BitbucketConstants.ApiKey),
                                        properties.getProperty(BitbucketConstants.ApiSecret));
    }

    public Properties properties() {
        return properties;
    }
}
