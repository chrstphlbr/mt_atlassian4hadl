package net.laaber.mt.hadl.atlassian.sensor;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabObjectSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollabSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.ICollaboratorSensor;
import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.ModelTypesUtil;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.InsufficientModelInformationException;
import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.TCollaborator;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import net.laaber.mt.hadl.atlassian.HadlConstants;

import javax.inject.Inject;

/**
 * Created by Christoph on 03.11.15.
 */
public class AtlassianSensorFactory implements SensorFactory {

    private final ModelTypesUtil mtu;

    @Inject
    public  AtlassianSensorFactory(ModelTypesUtil mtu) {
        this.mtu = mtu;
    }

    @Override
    public ICollabSensor getInstance(final THADLarchElement thadLarchElement) throws InsufficientModelInformationException {
        if (thadLarchElement instanceof TCollaborator) {
            return getInstance((TCollaborator) thadLarchElement);
        } else if (thadLarchElement instanceof TCollabObject) {
            return getInstance((TCollabObject) thadLarchElement);
        } else {
            return null;
        }
    }

    @Override
    public ICollaboratorSensor getInstance(final TCollaborator tCollaborator) throws InsufficientModelInformationException {
        // no collaborator sensors specified
        return null;
    }

    @Override
    public ICollabObjectSensor getInstance(final TCollabObject tCollabObject) throws InsufficientModelInformationException {
        switch (tCollabObject.getId()) {
            case HadlConstants.Obj.Wiki:
                return new WikiSensor(mtu);
            default:
                return null;
        }
    }
}
