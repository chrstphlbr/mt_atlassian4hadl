package net.laaber.mt.hadl.atlassian.data.bitbucket;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

import java.util.Date;
import java.util.Map;

/**
 * Created by Christoph on 23.10.15.
 */
public class User {
    public User() {}

    @Key
    private String username;
    @Key("display_name")
    private String displayName;
    @Key
    private String website;
    @Key
    private String uuid;
    @Key
    private GenericJson links;
    @Key("created_on")
    private String createdOn;
    @Key
    private String location;
    @Key
    private String type;

    public String getUsername() {
        return username;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(final String displayName) {
        this.displayName = displayName;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(final String website) {
        this.website = website;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    public GenericJson getLinks() {
        return links;
    }

    public void setLinks(final GenericJson links) {
        this.links = links;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(final String createdOn) {
        this.createdOn = createdOn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(final String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", displayName='" + displayName + '\'' +
                ", website='" + website + '\'' +
                ", uuid='" + uuid + '\'' +
                ", links=" + links +
                ", createdOn=" + createdOn +
                ", location='" + location + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
