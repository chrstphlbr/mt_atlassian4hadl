//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2015.11.03 um 11:57:26 AM CET 
//


package net.laaber.mt.hadl.atlassian.resourceDescriptor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import at.ac.tuwien.dsg.hadl.schema.runtime.TResourceDescriptor;


/**
 * <p>Java-Klasse für tAtlassianResourceDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="tAtlassianResourceDescriptor">
 *   &lt;complexContent>
 *     &lt;extension base="{http://at.ac.tuwien.dsg/hADL/hADLruntime}tResourceDescriptor">
 *       &lt;sequence>
 *         &lt;element name="bitbucket" type="{http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket}tBitbucketResourceDescriptor"/>
 *         &lt;element name="hipChat" type="{http://at.ac.tuwien.dsg/hADL/hADLresource-bitbucket}tHipChatResourceDescriptor"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tAtlassianResourceDescriptor", propOrder = {
    "bitbucket",
    "hipChat"
})
public class TAtlassianResourceDescriptor
    extends TResourceDescriptor
{

    @XmlElement(required = true, nillable = true)
    protected TBitbucketResourceDescriptor bitbucket;
    @XmlElement(required = true, nillable = true)
    protected THipChatResourceDescriptor hipChat;

    /**
     * Ruft den Wert der bitbucket-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link TBitbucketResourceDescriptor }
     *     
     */
    public TBitbucketResourceDescriptor getBitbucket() {
        return bitbucket;
    }

    /**
     * Legt den Wert der bitbucket-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link TBitbucketResourceDescriptor }
     *     
     */
    public void setBitbucket(TBitbucketResourceDescriptor value) {
        this.bitbucket = value;
    }

    /**
     * Ruft den Wert der hipChat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link THipChatResourceDescriptor }
     *     
     */
    public THipChatResourceDescriptor getHipChat() {
        return hipChat;
    }

    /**
     * Legt den Wert der hipChat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link THipChatResourceDescriptor }
     *     
     */
    public void setHipChat(THipChatResourceDescriptor value) {
        this.hipChat = value;
    }

}
