package net.laaber.mt.hadl.atlassian.surrogate;

import at.ac.tuwien.dsg.hadl.framework.runtime.impl.OperationalStateTransitionControl;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.*;
import at.ac.tuwien.dsg.hadl.schema.core.TAction;
import at.ac.tuwien.dsg.hadl.schema.core.TActivityScope;
import at.ac.tuwien.dsg.hadl.schema.core.TObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.*;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.HadlConstants;
import net.laaber.mt.hadl.atlassian.Util;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TBitbucketResourceDescriptor;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import net.laaber.mt.hadl.atlassian.rest.BitbucketClient;
import rx.subjects.BehaviorSubject;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.util.AbstractMap;
import java.util.Map;

/**
 * Created by Christoph on 29.10.15.
 */
public class WikiSurrogate extends AbstractAsyncObjectSurrogate {
    private static final Duration waitForWiki = Duration.ofMinutes(2);
    private static final Duration checkForWiki = Duration.ofSeconds(30);

    private static final String propTeamname = "bb.teamname";
    private static final String propRepository = "bb.repository";
    private static final String propAdminMail = "bb.admin.login";
    private static final String propGmailUser = "gmail.user";
    private static final String propGmailPwd = "gmail.pwd";

    private TBitbucketResourceDescriptor rd;
    private BitbucketClient bbClient;
    private Wiki wiki;
    private String teamname;
    private String repository;
    private String adminMail;
    private String gmailUser;
    private String gmailPwd;

    private Map.Entry<TBitbucketResourceDescriptor, TOperationalObjectRef> next;
    private Map.Entry<TBitbucketResourceDescriptor, TOperationalObjectRef> prev;

    public WikiSurrogate() {}

    private boolean sendMail(String subject, String content) {
        return Util.sendGmailMail(gmailUser, gmailPwd, gmailUser, adminMail, subject, content);
    }

    @Override
    public void asyncBegin(final BehaviorSubject<SurrogateEvent> s) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        updateNext(s);
        updatePrevious(s);
        s.onCompleted();
    }

    private void updateNext(BehaviorSubject<SurrogateEvent> s) {
        if (next != null) {
            final String nextWikiName = next.getKey().getBitbucketId();
            final Option<Wiki> nextWiki = bbClient.wiki(teamname, repository, nextWikiName);
            if (nextWiki.isNone()) {
                s.onError(new SurrogateException("no next wiki '" + nextWikiName + "' exists on collab platform", SurrogateErrorType.NOT_POSSIBLE));
            }

            boolean suc = sendMail("Add next Wiki", "Add next Wiki '" + nextWikiName + "' to " + rd.getBitbucketId());
            if (!suc) {
                s.onError(new SurrogateException("could not send mail", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC));
            }

            final Option<Map.Entry<String, String>> prevNext = Util.previousNext(wiki.getData());
            String expectedNext = null;
            if (prevNext.isSome()) {
                expectedNext = prevNext.some().getValue();
            }
            final boolean wait = waitForWiki("n", rd.getBitbucketId(), expectedNext);

            if (wait) {
                next.getValue().setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.DESCRIBED_EXISTING));
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS));
            } else {
                next.getValue().setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_EXISTING_DENIED));
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_FAILED, new SurrogateException("could not add next wiki to '" + rd.getBitbucketId() + "'", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), next.getValue()));
            }
            next = null;
        }
    }

    private void updatePrevious(BehaviorSubject<SurrogateEvent> s) {
        if (prev != null) {
            final String prevWikiName = prev.getKey().getBitbucketId();
            final Option<Wiki> prevWiki = bbClient.wiki(teamname, repository, prevWikiName);
            if (prevWiki.isNone()) {
                s.onError(new SurrogateException("no previous wiki '" + prevWikiName + "' exists on collab platform", SurrogateErrorType.NOT_POSSIBLE));
            }
            boolean suc = sendMail("Add previous Wiki", "Add previous Wiki '" + prev.getKey().getBitbucketId() + "' to " + rd.getBitbucketId());
            if (!suc) {
                s.onError(new SurrogateException("could not send mail", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC));
            }

            final Option<Map.Entry<String, String>> prevNext = Util.previousNext(wiki.getData());
            String expectedPrev = null;
            if (prevNext.isSome()) {
                expectedPrev = prevNext.some().getKey();
            }
            final boolean wait = waitForWiki("p", rd.getBitbucketId(), expectedPrev);

            if (wait) {
                prev.getValue().setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.DESCRIBED_EXISTING));
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_SUCCESS));
            } else {
                prev.getValue().setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_EXISTING_DENIED));
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STARTING_FAILED, new SurrogateException("could not add next wiki to '" + rd.getBitbucketId() + "'", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), prev.getValue()));
            }
            prev = null;
        }
    }

    private boolean waitForWiki(String type, String wikiName, String expectedValue) {
        LocalTime start = LocalTime.now();
        Option<Map.Entry<String, String>> prevNext;
        while (LocalTime.now().isBefore(start.plus(waitForWiki))) {
            prevNext = Util.previousNext(bbClient, teamname, repository, wikiName);
            if (prevNext.isSome()) {
                if ("p".equals(type)) {
                    if (expectedValue == null && prevNext.some().getKey() != null) {
                        return true;
                    } else if (expectedValue != null && expectedValue.equals(prevNext.some().getKey())) {
                        return true;
                    }
                } else if ("n".equals(type)) {
                    if (expectedValue == null && prevNext.some().getValue() != null) {
                        return true;
                    } else if (expectedValue != null && expectedValue.equals(prevNext.some().getValue())) {
                        return true;
                    }
                } else {
                    return false;
                }
            }
            try {
                Thread.sleep(checkForWiki.toMillis());
            } catch (InterruptedException e) {
                break;
            }
        }
        return false;
    }

    @Override
    public void asyncStop(final BehaviorSubject<SurrogateEvent> s) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        Option<Wiki> optWiki = bbClient.wiki(teamname, repository, rd.getBitbucketId());
        if (optWiki.isNone()) {
            s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_FAILED, new SurrogateException("Could not reload wiki '" + rd.getBitbucketId() + "'", SurrogateErrorType.COLLAB_PLATFORM_SPECIFIC), this.oc));
        } else {
            Wiki newWiki = optWiki.some();
            newWiki.setNext(wiki.getNext());
            newWiki.setPrevious(wiki.getPrevious());
            wiki = newWiki;
            s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.STOPPING_SUCCESS, this.oc));
        }
        s.onCompleted();
    }

    @Override
    public void asyncRelease(final BehaviorSubject<SurrogateEvent> s) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // do not delete wiki
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELEASING_SUCCESS));
        s.onCompleted();
    }

    @Override
    public void asyncAcquire(final TActivityScope scope, final TOperationalObject o, final BehaviorSubject<SurrogateEvent> s) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        Option<TBitbucketResourceDescriptor> optRd = Util.firstResorurceDescriptorOfType(TBitbucketResourceDescriptor.class, o.getResourceDescriptor());
        if (optRd.isNone()) {
            this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            s.onError(new SurrogateException("No TBitbucketResourceDescriptor found", SurrogateErrorType.NO_MATCHING_RESOURCE_DESCRIPTOR));
            return;
        }
        rd = optRd.some();

        try {
            AtlassianClientFactory f = AtlassianClientFactory.instance();
            bbClient = f.bitbucketClient();
            teamname = f.properties().getProperty(propTeamname);
            repository = f.properties().getProperty(propRepository);
            adminMail = f.properties().getProperty(propAdminMail);
            gmailUser = f.properties().getProperty(propGmailUser);
            gmailPwd = f.properties().getProperty(propGmailPwd);
        } catch (IOException e) {
            e.printStackTrace();
            this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.PRESCRIBED_NONEXISTING));
            s.onError(new SurrogateException("Could not load credentials", SurrogateErrorType.INCORRECT_CONFIGURATION));
            return;
        }

        Option<Wiki> optWiki = bbClient.wiki(teamname, repository, rd.getBitbucketId());
        if (optWiki.isNone()) {
            // create wiki
            boolean success = bbClient.createWiki(teamname, repository, rd.getBitbucketId(), "Wiki for " + rd.getBitbucketId());

            if (!success) {
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_FAILED, o));
                s.onCompleted();
                return;
            }

            optWiki = bbClient.wiki(teamname, repository, rd.getBitbucketId());
            if (optWiki.isNone()) {
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_FAILED, o));
                s.onCompleted();
                return;
            }
        }

        wiki = optWiki.some();
        this.oc.setState(OperationalStateTransitionControl.doTransition(TOperationalState.PRESCRIBED_EXISTING, TOperationalState.DESCRIBED_EXISTING));
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.ACQUIRING_SUCCESS, this.oc));
        s.onCompleted();
    }

    private boolean createWikiViaMail() {
        sendMail("Create Wiki", "Create Wiki '" + rd.getBitbucketId() + "' in Repo '" + repository + "' as '" + teamname + "'");
        LocalTime start = LocalTime.now();
        Option<Wiki> optWiki;
        while (LocalTime.now().isBefore(start.plus(waitForWiki))) {
            optWiki = bbClient.wiki(teamname, repository, rd.getBitbucketId());
            if (optWiki.isSome()) {
                wiki = optWiki.some();
                return true;
            }
            try {
                Thread.sleep(checkForWiki.toMillis());
            } catch (InterruptedException e) {
                break;
            }
        }
        return false;
    }

    @Override
    public void asyncLinkTo(final TAction ownAction, final TOperationalConnector oppositeConn, final TAction oppositeAction, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // no links
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_FAILED, new SurrogateException("WikiSurrogate does not support linking", SurrogateErrorType.NOT_POSSIBLE), link));
        s.onCompleted();
    }

    @Override
    public void asyncLinkTo(final TAction ownAction, final TOperationalComponent oppositeComp, final TAction oppositeAction, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // no links
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_FAILED, new SurrogateException("WikiSurrogate does not support linking", SurrogateErrorType.NOT_POSSIBLE), link));
        s.onCompleted();
    }

    @Override
    public void asyncDisconnectFrom(final TAction ownAction, final TOperationalComponent oppositeComp, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // no links
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_FAILED, new SurrogateException("WikiSurrogate does not support unlinking", SurrogateErrorType.NOT_POSSIBLE), link));
        s.onCompleted();
    }

    @Override
    public void asyncDisconnectFrom(final TAction ownAction, final TOperationalConnector oppositeConn, final BehaviorSubject<SurrogateEvent> s, final TOperationalCollabLink link) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        // no links
        s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.REWIRING_FAILED, new SurrogateException("WikiSurrogate does not support unlinking", SurrogateErrorType.NOT_POSSIBLE), link));
        s.onCompleted();
    }

    @Override
    public void asyncRelating(final TOperationalObject to, final TObjectRef refType, final boolean origin, final boolean remove, final BehaviorSubject<SurrogateEvent> s, final TOperationalObjectRef ref) {
        if (ESurrogateStatus.RELEASING_SUCCESS.equals(this.getLastStatus())) {
            s.onCompleted();
            return;
        }
        switch (refType.getId()) {
            case HadlConstants.ObjRef.NextWiki:
                if (!remove) {
                    if (origin) {
                        Option<TBitbucketResourceDescriptor> optRd = Util.firstResorurceDescriptorOfType(TBitbucketResourceDescriptor.class, to.getResourceDescriptor());
                        if (optRd.isNone()) {
                            s.onError(new SurrogateException("No TBitbucketResourceDescriptor for 'to'", SurrogateErrorType.INCORRECT_CONFIGURATION));
                            return;
                        }
                        next = new AbstractMap.SimpleImmutableEntry<>(optRd.some(), ref);
                    }
                    s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
                } else {
                    // no removal on collab platform
                    ref.setState(OperationalStateTransitionControl.doTransition(TOperationalState.DESCRIBED_EXISTING, TOperationalState.DESCRIBED_NONEXISTING));
                    s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
                }
                break;
            case HadlConstants.ObjRef.PrevWiki:
                if (!remove) {
                    if (origin) {
                        Option<TBitbucketResourceDescriptor> optRd = Util.firstResorurceDescriptorOfType(TBitbucketResourceDescriptor.class, to.getResourceDescriptor());
                        if (optRd.isNone()) {
                            s.onError(new SurrogateException("No TBitbucketResourceDescriptor for 'to'", SurrogateErrorType.INCORRECT_CONFIGURATION));
                            return;
                        }
                        prev = new AbstractMap.SimpleImmutableEntry<>(optRd.some(), ref);
                    }
                    s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
                } else {
                    // no removal on collab platform
                    ref.setState(OperationalStateTransitionControl.doTransition(TOperationalState.DESCRIBED_EXISTING, TOperationalState.DESCRIBED_NONEXISTING));
                    s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_SUCCESS, ref));
                }
                break;
            default:
                s.onNext(new SurrogateEvent(thisRef, ESurrogateStatus.RELATING_FAILED, new SurrogateException("Unknown reference type '" + refType.getId() + "'", SurrogateErrorType.NOT_POSSIBLE), ref));
                break;
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        s.onCompleted();
    }
}
