package net.laaber.mt.hadl.atlassian.data.bitbucket;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 27.10.15.
 */
public class SuccessResponse {
    public SuccessResponse() {}

    @Key
    private int pagelen;
    @Key
    private List<GenericJson> values;
    @Key
    private int page;
    @Key
    private int size;

    public int getPagelen() {
        return pagelen;
    }

    public void setPagelen(final int pagelen) {
        this.pagelen = pagelen;
    }

    public int getPage() {
        return page;
    }

    public void setPage(final int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(final int size) {
        this.size = size;
    }

    public List<GenericJson> getValues() {
        return values;
    }

    public void setValues(final List<GenericJson> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "SuccessResponse{" +
                "pagelen=" + pagelen +
                ", values='" + values + '\'' +
                ", page=" + page +
                ", size=" + size +
                '}';
    }
}
