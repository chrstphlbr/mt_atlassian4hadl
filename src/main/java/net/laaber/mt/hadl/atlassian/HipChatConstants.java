package net.laaber.mt.hadl.atlassian;

/**
 * Created by Christoph on 27.10.15.
 */
public final class HipChatConstants {
    private HipChatConstants() {}

    private static final String BaseUrl = "https://api.hipchat.com/";

    public static final class APIv2 {
        private APIv2() {}

        public static final String BaseUrl = HipChatConstants.BaseUrl + "v2";
        public static final String Room = "room";
        public static final String User = "user";
        public static final String Member = "member";
    }

    // atlassianCredentials.xml keys
    public static final String AccessToken = "hc.accessToken";
}
