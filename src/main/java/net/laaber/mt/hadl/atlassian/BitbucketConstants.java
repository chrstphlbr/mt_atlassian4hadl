package net.laaber.mt.hadl.atlassian;

/**
 * Created by Christoph on 27.10.15.
 */
public final class BitbucketConstants {

    private BitbucketConstants() {}

    public static final String accessTokenUrl = "https://bitbucket.org/site/oauth2/access_token";
    public static final String authorizeUrl = "https://bitbucket.org/site/oauth2/authorize";

    // API v1 constants
    public static final class APIv1 {
        private APIv1() {}

        public static final String BaseUrl = "https://bitbucket.org/api/1.0";
        public static final String Repositories = "repositories";
        public static final String Wiki = "wiki";
    }

    // API v2 constants
    public static final class APIv2 {
        private APIv2() {}

        public static final String BaseUrl = "https://api.bitbucket.org/2.0";
        public static final String Repositories = "repositories";
        public static final String Teams = "teams";
        public static final String TeamMembers = "members";
        public static final String Users = "users";
    }

    // atlassianCredentials.xml keys
    public static final String ApiKey = "bb.apiKey";
    public static final String ApiSecret = "bb.apiSecret";
    public static final String AdminLogin = "bb.admin.login";
    public static final String AdminPassword = "bb.admin.password";
    public static final String Teamname = "bb.teamname";
    public static final String Repository = "bb.repository";

}
