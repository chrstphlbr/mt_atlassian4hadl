package net.laaber.mt.hadl.atlassian.rest;

import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.HipChatConstants;
import net.laaber.mt.hadl.atlassian.Util;
import net.laaber.mt.hadl.atlassian.data.hipChat.Chat;
import net.laaber.mt.hadl.atlassian.data.hipChat.Privacy;
import net.laaber.mt.hadl.atlassian.data.hipChat.SuccessResponse;
import net.laaber.mt.hadl.atlassian.data.hipChat.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christoph on 27.10.15.
 */
public class HipChatClientImpl implements HipChatClient {
    private static final HttpTransport httpTransport = new NetHttpTransport();
    private static final JsonFactory jsonFactory = new JacksonFactory();

    private final String accessToken;

    public HipChatClientImpl(String accessToken) {
        this.accessToken = accessToken;
    }

    private HttpRequestFactory requestFactory() {
        Credential c = new Credential(BearerToken.authorizationHeaderAccessMethod())
                .setAccessToken(accessToken);
        HttpRequestFactory f = httpTransport.createRequestFactory(new HttpRequestInitializer() {
            @Override
            public void initialize(final HttpRequest httpRequest) throws IOException {
                c.initialize(httpRequest);
                httpRequest.setParser(jsonFactory.createJsonObjectParser());
            }
        });
        return f;
    }

    @Override
    public Option<String> createChat(final String name, final String topic, final Privacy privacy, final User owner, final boolean guestAccess) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.Room);

        Chat c = new Chat();
        c.setName(name);
        c.setTopic(topic);
        c.setPrivacy(privacy.toString());
        c.setOwner(owner);
        c.setIsGuestAccessible(guestAccess);

        try {
            HttpRequest req = f.buildPostRequest(new GenericUrl(url), new JsonHttpContent(jsonFactory, c));
            Chat chat = req.execute().parseAs(Chat.class);
            return Option.some(String.valueOf(chat.getId()));
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public boolean deleteChat(final String idOrName) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.Room, idOrName);
        try {
            final HttpRequest req = f.buildDeleteRequest(new GenericUrl(url));
            HttpResponse res = req.execute();
            return res.isSuccessStatusCode();
        } catch (IOException e) {
//            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Option<Chat> chat(final String idOrName) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.Room, idOrName);
        try {
            final HttpRequest req = f.buildGetRequest(new GenericUrl(url));
            final Chat c = req.execute().parseAs(Chat.class);
            return Option.some(c);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public Option<List<User>> usersInChat(final String roomIdOrName) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.Room, roomIdOrName, HipChatConstants.APIv2.Member);
        try {
            final HttpRequest req = f.buildGetRequest(new GenericUrl(url));
            SuccessResponse response = req.execute().parseAs(SuccessResponse.class);
            List<User> users = new ArrayList<>();
            jsonFactory.createJsonParser(response.getItems().toString()).parseArrayAndClose(users, User.class);
            return Option.some(users);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }

    @Override
    public boolean inviteUserToChat(final String userIdOrEmail, final String roomIdOrEmail) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.Room, roomIdOrEmail, HipChatConstants.APIv2.Member, userIdOrEmail);
        try {
            final HttpRequest req = f.buildPutRequest(new GenericUrl(url), new EmptyContent());
            HttpResponse res = req.execute();
            return res.isSuccessStatusCode();
        } catch (IOException e) {
//            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean removeUserFromChat(final String userIdOrEmail, final String roomIdOrEmail) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.Room, roomIdOrEmail, HipChatConstants.APIv2.Member, userIdOrEmail);
        try {
            final HttpRequest req = f.buildDeleteRequest(new GenericUrl(url));
            HttpResponse res = req.execute();
            return res.isSuccessStatusCode();
        } catch (IOException e) {
//            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Option<User> user(final String idOrEmail) {
        HttpRequestFactory f = requestFactory();
        String url = Util.url(HipChatConstants.APIv2.BaseUrl, HipChatConstants.APIv2.User, idOrEmail);
        try {
            final HttpRequest req = f.buildGetRequest(new GenericUrl(url));
            final User user = req.execute().parseAs(User.class);
            return Option.some(user);
        } catch (IOException e) {
//            e.printStackTrace();
            return Option.none();
        }
    }
}
