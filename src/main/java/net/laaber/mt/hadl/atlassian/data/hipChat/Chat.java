package net.laaber.mt.hadl.atlassian.data.hipChat;

import com.google.api.client.json.GenericJson;
import com.google.api.client.util.Key;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLongArray;

/**
 * Created by Christoph on 22.10.15.
 */
public class Chat implements Cloneable {
    public Chat() {}

    @Key("xmpp_jid")
    private String xmppJid;
    @Key
    private GenericJson statistics;
    @Key
    private String name;
    @Key
    private GenericJson links;
    @Key
    private String created;
    @Key("is_archived")
    private boolean isArchived;
    @Key
    private String privacy;
    @Key("is_guest_accessible")
    private boolean isGuestAccessible;
    @Key
    private String topic;
    @Key
    private List<User> participants;
    @Key("avatar_url")
    private String avatarUrl;
    @Key
    private String version;
    @Key
    private User owner;
    @Key
    private long id;
    @Key("guest_access_url")
    private String guestAccessUrl;

    public String getXmppJid() {
        return xmppJid;
    }

    public void setXmppJid(final String xmppJid) {
        this.xmppJid = xmppJid;
    }

    public GenericJson getStatistics() {
        return statistics;
    }

    public void setStatistics(final GenericJson statistics) {
        this.statistics = statistics;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public GenericJson getLinks() {
        return links;
    }

    public void setLinks(final GenericJson links) {
        this.links = links;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(final String created) {
        this.created = created;
    }

    public boolean isArchived() {
        return isArchived;
    }

    public void setIsArchived(final boolean isArchived) {
        this.isArchived = isArchived;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(final String privacy) {
        this.privacy = privacy;
    }

    public boolean isGuestAccessible() {
        return isGuestAccessible;
    }

    public void setIsGuestAccessible(final boolean isGuestAccessible) {
        this.isGuestAccessible = isGuestAccessible;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(final String topic) {
        this.topic = topic;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(final List<User> participants) {
        this.participants = participants;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(final String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(final User owner) {
        this.owner = owner;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getGuestAccessUrl() {
        return guestAccessUrl;
    }

    public void setGuestAccessUrl(final String guestAccessUrl) {
        this.guestAccessUrl = guestAccessUrl;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Chat chat = (Chat) o;

        return id == chat.id || name.equals(chat.name);

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Chat{" +
                "xmppJid='" + xmppJid + '\'' +
                ", statistics=" + statistics +
                ", name='" + name + '\'' +
                ", links=" + links +
                ", created='" + created + '\'' +
                ", isArchived=" + isArchived +
                ", privacy='" + privacy + '\'' +
                ", isGuestAccessible=" + isGuestAccessible +
                ", topic='" + topic + '\'' +
                ", participants=" + participants +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", version='" + version + '\'' +
                ", owner=" + owner +
                ", id=" + id +
                ", guestAccessUrl='" + guestAccessUrl + '\'' +
                '}';
    }

    @Override
    public Chat clone() {
        Chat c = new Chat();
        c.setAvatarUrl(avatarUrl);
        c.setCreated(created);
        c.setGuestAccessUrl(guestAccessUrl);
        c.setId(id);
        c.setIsArchived(isArchived);
        c.setIsGuestAccessible(isGuestAccessible);
        c.setLinks(links.clone());
        c.setName(name);
        c.setParticipants(new ArrayList<>(c.participants));
        c.setPrivacy(privacy);
        c.setStatistics(statistics.clone());
        c.setTopic(topic);
        c.setVersion(version);
        c.setXmppJid(xmppJid);
        return c;
    }
}
