package net.laaber.mt.hadl.atlassian.rest;

import fj.data.Option;
import net.laaber.mt.hadl.atlassian.BaseTest;
import net.laaber.mt.hadl.atlassian.HipChatConstants;
import net.laaber.mt.hadl.atlassian.data.hipChat.Chat;
import net.laaber.mt.hadl.atlassian.data.hipChat.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

/**
 * Created by Christoph on 27.10.15.
 */
public class HipChatClientTest extends BaseTest {

    private static final String chatName = "TestChat";
    private static HipChatClient c;

    @BeforeClass
    public static void setUpClass() {
        String accessToken = properties.getProperty(HipChatConstants.AccessToken);
        Assert.assertNotNull("HipChat access token is null", accessToken);

        c = new HipChatClientImpl(accessToken);
        try {
            deleteChat(chatName);
        } catch (Throwable t) {
            System.out.println("chat not deleted");
        }
    }

    @After
    public void removeChat() {
        try {
            deleteChat(chatName);
        } catch (Throwable t) {
            System.out.println("chat not deleted");
        }
    }

    @Test
    public void user() {
        String userEmail = "christoph.laaber@gmail.com";
        Option<User> user  = c.user(userEmail);
        Assert.assertFalse("No user provided", user.isNone());
        User u = user.some();
        Assert.assertTrue("User email not correct", userEmail.equals(u.getEmail()));
    }

    @Test
    public void createRemoveByIdChat() {
        createChat();
    }

    private String createChat() {
        String chatName = HipChatClientTest.chatName;
        Option<String> createResult = c.createChat(chatName);
        Assert.assertFalse("No chat created", createResult.isNone());
        String chatId = createResult.some();
        Assert.assertNotNull("chatId is null", chatId);
        return chatId;
    }

    private static boolean deleteChat(String id) {
        boolean success = c.deleteChat(id);
        Assert.assertTrue("Chat not deleted", success);
        return success;
    }

    @Test
    public void chat() {
        String id = createChat();

        Option<Chat> result = c.chat(chatName);
        Assert.assertFalse("no result", result.isNone());
        Chat c = result.some();
        Assert.assertNotNull("chat is null", c);
        Assert.assertEquals("wrong chat", String.valueOf(c.getId()), id);
    }

    @Test
    public void inviteUserToChat() {
        String u1 = "hadltestuser2@gmail.com";
        String u2 = "hadltestuser1@gmail.com";
        inviteUsersToChat(u1, u2);
    }

    private void inviteUsersToChat(String... users) {
        createChat();

        Option<List<User>> u = c.usersInChat(chatName);
        Assert.assertFalse("no usersInChat result", u.isNone());
        Assert.assertTrue("incorrect number of users in room (exp: 1, was: " + u.some().size() + ")", 1 == u.some().size());

        for (int i = 0; i < users.length; i++) {
            String userId = users[i];
            boolean success = c.inviteUserToChat(userId, chatName);
            Assert.assertTrue("could not invite user (" + userId + ") to chat", success);

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        u = c.usersInChat(chatName);
        Assert.assertFalse("no usersInChat result", u.isNone());
        Assert.assertEquals("incorrect number of users in room", users.length + 1, u.some().size());
    }

    @Test
    public void removeUsersFromChat() {
        String u1 = "hadltestuser2@gmail.com";
        String u2 = "hadltestuser1@gmail.com";
        inviteUsersToChat(u1, u2);

        boolean success = c.removeUserFromChat(u1, chatName);
        Assert.assertTrue("could not remove user from chat", success);


        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        success = c.removeUserFromChat(u2, chatName);
        Assert.assertTrue("could not remove user from chat", success);


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Option<List<User>> u = c.usersInChat(chatName);
        Assert.assertFalse("no usersInChat result", u.isNone());
        Assert.assertTrue("incorrect number of users in room (exp: 1, was: " + u.some().size() + ")", 1 == u.some().size());
    }
}
