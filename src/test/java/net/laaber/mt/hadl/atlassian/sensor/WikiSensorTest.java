package net.laaber.mt.hadl.atlassian.sensor;

import at.ac.tuwien.dsg.hadl.schema.core.TCollabObject;
import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import fj.data.Either;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.BaseTest;
import net.laaber.mt.hadl.atlassian.BitbucketConstants;
import net.laaber.mt.hadl.atlassian.HadlConstants;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TBitbucketResourceDescriptor;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Christoph on 10.11.15.
 *
 * DELETE Wikis from Bitbucket with "SprintWiki-" prefix
 */
public class WikiSensorTest extends BaseTest {

    private static String teamname;
    private static String repo;

    private static final String tp1 = "SprintWiki-1";
    private static final String tp2 = "SprintWiki-2";
    private static final String tp3 = "SprintWiki-3";

    @BeforeClass
    public static void setUpClass() throws IOException {
        teamname = properties.getProperty(BitbucketConstants.Teamname);
        repo = properties.getProperty(BitbucketConstants.Repository);

        boolean res = false;
        Option<Wiki> wikiResult = bbClient.wiki(teamname, repo, tp1);
        if (wikiResult.isNone()) {
            res = bbClient.createWiki(teamname, repo, tp1, "!!,SprintWiki-2\nTest Page 1 content");
            Assert.assertTrue("Could not create " + tp1, res);
        }
        wikiResult = bbClient.wiki(teamname, repo, tp2);
        if (wikiResult.isNone()) {
            res = bbClient.createWiki(teamname, repo, tp2, "!!SprintWiki-1,SprintWiki-3\nTest Page 2 content");
            Assert.assertTrue("Could not create " + tp2, res);
        }
        wikiResult = bbClient.wiki(teamname, repo, tp3);
        if (wikiResult.isNone()) {
            res = bbClient.createWiki(teamname, repo, tp3, "!!SpintWiki-2,\nTest Page 3 content");
            Assert.assertTrue("Could not create " + tp3, res);
        }
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("delete 'SprintWiki-' repos on bitbucket");
    }

    @Test
    public void previousWikiExisting() {
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Wiki, AtlassianResourceDescriptorFactory.bitbucket(tp2, tp2));
        handleError(acquireResult);
        TOperationalObject wiki = (TOperationalObject) acquireResult.right().value();

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Obj.Wiki, HadlConstants.ObjRef.PrevWiki);
        final Either<Throwable, List<THADLarchElement>> loadResult = ps.getMonitor().load(what, wiki, null);
        handleError(loadResult);

        List<THADLarchElement> load = loadResult.right().value();
        Assert.assertEquals("Invalid result size. Expected 1, got " + load.size(), load.size(), 1);
        final THADLarchElement prev = load.get(0);
        if (!(prev instanceof TOperationalObject)) {
            Assert.fail("prev not of TOperationalObject");
        }
        final TOperationalObject p = (TOperationalObject) prev;
        final TCollabObject type = (TCollabObject) ps.getModelTypesUtil().getByTypeRef(p.getInstanceOf());
        Assert.assertEquals("incorrect hADL type", HadlConstants.Obj.Wiki, type.getId());
        Assert.assertEquals("Incorrect name", tp1, ((TBitbucketResourceDescriptor) p.getResourceDescriptor().get(0)).getBitbucketId());
    }

    @Test
    public void previousWikiNotExisting() {
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Wiki, AtlassianResourceDescriptorFactory.bitbucket(tp1, tp1));
        handleError(acquireResult);
        TOperationalObject wiki = (TOperationalObject) acquireResult.right().value();

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Obj.Wiki, HadlConstants.ObjRef.PrevWiki);
        final Either<Throwable, List<THADLarchElement>> loadResult = ps.getMonitor().load(what, wiki, null);
        handleError(loadResult);

        List<THADLarchElement> load = loadResult.right().value();
        Assert.assertEquals("Invalid result size. Expected 0, got " + load.size(), 0, load.size());
    }

    @Test
    public void nextWikiExisting() {
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Wiki, AtlassianResourceDescriptorFactory.bitbucket(tp2, tp2));
        handleError(acquireResult);
        TOperationalObject wiki = (TOperationalObject) acquireResult.right().value();

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Obj.Wiki, HadlConstants.ObjRef.NextWiki);
        final Either<Throwable, List<THADLarchElement>> loadResult = ps.getMonitor().load(what, wiki, null);
        handleError(loadResult);

        List<THADLarchElement> load = loadResult.right().value();
        Assert.assertEquals("Invalid result size. Expected 1, got " + load.size(), load.size(), 1);
        final THADLarchElement prev = load.get(0);
        if (!(prev instanceof TOperationalObject)) {
            Assert.fail("prev not of TOperationalObject");
        }
        final TOperationalObject p = (TOperationalObject) prev;
        final TCollabObject type = (TCollabObject) ps.getModelTypesUtil().getByTypeRef(p.getInstanceOf());
        Assert.assertEquals("incorrect hADL type", HadlConstants.Obj.Wiki, type.getId());
        Assert.assertEquals("Incorrect name", tp3, ((TBitbucketResourceDescriptor) p.getResourceDescriptor().get(0)).getBitbucketId());
    }

    @Test
    public void nextWikiNotExisting() {
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Wiki, AtlassianResourceDescriptorFactory.bitbucket(tp3, tp3));
        handleError(acquireResult);
        TOperationalObject wiki = (TOperationalObject) acquireResult.right().value();

        Map.Entry<String, String> what = new AbstractMap.SimpleImmutableEntry<String, String>(HadlConstants.Obj.Wiki, HadlConstants.ObjRef.NextWiki);
        final Either<Throwable, List<THADLarchElement>> loadResult = ps.getMonitor().load(what, wiki, null);
        handleError(loadResult);

        List<THADLarchElement> load = loadResult.right().value();
        Assert.assertEquals("Invalid result size. Expected 0, got " + load.size(), 0, load.size());
    }
}
