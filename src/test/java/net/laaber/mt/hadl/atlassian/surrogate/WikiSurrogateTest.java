package net.laaber.mt.hadl.atlassian.surrogate;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObjectRef;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import fj.data.Either;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.BaseTest;
import net.laaber.mt.hadl.atlassian.BitbucketConstants;
import net.laaber.mt.hadl.atlassian.HadlConstants;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Christoph on 12.11.15.
 *
 * delete TestPage2 and TestPage3
 */
public class WikiSurrogateTest extends BaseTest {

    private static String teamname;
    private static String repository;

    private Map<String, TOperationalObject> wikis = new HashMap<>();

    @BeforeClass
    public static void setUpClass() {
        teamname = properties.getProperty(BitbucketConstants.Teamname);
        repository = properties.getProperty(BitbucketConstants.Repository);
    }

    private TOperationalObject wiki(String name, String content) {
        TOperationalObject o = wikis.get(name);
        if (o != null) {
            return o;
        }

        final Either<Throwable, THADLarchElement> acquireRes = ps.getLinker().acquire(HadlConstants.Obj.Wiki, AtlassianResourceDescriptorFactory.bitbucket(name, content));
        handleError(acquireRes);
        o = (TOperationalObject) acquireRes.right().value();
        Assert.assertNotNull("no operational returned", o);
        wikis.put(name, o);
        return o;
    }

    @Test
    public void acquire() {
        String id = "TestPage2";
        final Either<Throwable, THADLarchElement> acquireRes = ps.getLinker().acquire(HadlConstants.Obj.Wiki, AtlassianResourceDescriptorFactory.bitbucket(id, id));
        handleError(acquireRes);
        TOperationalObject o = (TOperationalObject) acquireRes.right().value();
        Assert.assertNotNull("no operational returned", o);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        final Option<Wiki> wikiRes = bbClient.wiki(teamname, repository, id);
        Assert.assertTrue("no wiki returned", wikiRes.isSome());
        Wiki w = wikiRes.some();
        Assert.assertEquals("Incorrect chat returned", id, w.getPage());
        Assert.assertEquals("operational in incorrect state", TOperationalState.DESCRIBED_EXISTING, o.getState());
    }

    @Test
    public void acquireExisting() {
        String id = "TestPage3";
        TOperationalObject o = wiki(id, id);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        final Option<Wiki> wikiRes = bbClient.wiki(teamname, repository, id);
        Assert.assertTrue("no wiki returned", wikiRes.isSome());
        Wiki w = wikiRes.some();
        Assert.assertEquals("Incorrect chat returned", id, w.getPage());
        Assert.assertEquals("operational in incorrect state", TOperationalState.DESCRIBED_EXISTING, o.getState());
    }

    @Test
    public void setNext() {
        String w1Id = "TestPage3";
        TOperationalObject w1 = wiki(w1Id, w1Id);
        String w2Id = "TestPage4";
        TOperationalObject w2 = wiki(w2Id, w2Id);

        stopScope();

        final Either<Throwable, TOperationalObjectRef> referenceRes = ps.getLinker().reference(w1, w2, HadlConstants.ObjRef.NextWiki);
        handleError(referenceRes);
        TOperationalObjectRef ref = referenceRes.right().value();
        Assert.assertNotNull("no ref returned", ref);
        Assert.assertEquals("incorrect operational state", TOperationalState.PRESCRIBED_EXISTING, ref.getState());

        startScope();

        Assert.assertEquals("incorrect operational state", TOperationalState.DESCRIBED_EXISTING, ref.getState());

        final Option<Wiki> w1Res = bbClient.wiki(teamname, repository, w1Id);
        if (w1Res.isNone()) {
            Assert.fail("no wiki 1 returned");
        }
        Wiki w1Data = w1Res.some();
        Assert.assertEquals("incorrect wiki 1 page name returned", w1Id, w1Data.getPage());
        try {
            String next = w1Data.getData().split("\n")[0].substring(2).split(",")[1];
            Assert.assertEquals("incorrect next retrieved", w2Id, next);
        } catch (Throwable t) {
            Assert.fail(t.getMessage());
        }

        final Option<Wiki> w2Res = bbClient.wiki(teamname, repository, w2Id);
        if (w2Res.isNone()) {
            Assert.fail("no wiki 2 returned");
        }
        Wiki w2Data = w2Res.some();
        Assert.assertEquals("incorrect wiki 2 page name returned", w2Id, w2Data.getPage());
    }

    @Test
    public void setPrevious() {
        String w1Id = "TestPage3";
        TOperationalObject w1 = wiki(w1Id, w1Id);
        String w2Id = "TestPage4";
        TOperationalObject w2 = wiki(w2Id, w2Id);

        stopScope();

        final Either<Throwable, TOperationalObjectRef> referenceRes = ps.getLinker().reference(w2, w1, HadlConstants.ObjRef.PrevWiki);
        handleError(referenceRes);
        TOperationalObjectRef ref = referenceRes.right().value();
        Assert.assertNotNull("no ref returned", ref);
        Assert.assertEquals("incorrect operational state", TOperationalState.PRESCRIBED_EXISTING, ref.getState());

        startScope();

        Assert.assertEquals("incorrect operational state", TOperationalState.DESCRIBED_EXISTING, ref.getState());

        final Option<Wiki> w1Res = bbClient.wiki(teamname, repository, w1Id);
        if (w1Res.isNone()) {
            Assert.fail("no wiki 1 returned");
        }
        Wiki w1Data = w1Res.some();
        Assert.assertEquals("incorrect wiki 1 page name returned", w1Id, w1Data.getPage());

        final Option<Wiki> w2Res = bbClient.wiki(teamname, repository, w2Id);
        if (w2Res.isNone()) {
            Assert.fail("no wiki 2 returned");
        }
        Wiki w2Data = w2Res.some();
        Assert.assertEquals("incorrect wiki 2 page name returned", w2Id, w2Data.getPage());
        try {
            String prev = w2Data.getData().split("\n")[0].substring(2).split(",")[0];
            System.out.println(w1Id + "      " + prev);
            Assert.assertEquals("incorrect next retrieved", w1Id, prev);
        } catch (Throwable t) {
            Assert.fail(t.getMessage());
        }
    }

}
