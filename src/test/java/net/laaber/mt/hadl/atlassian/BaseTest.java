package net.laaber.mt.hadl.atlassian;

import at.ac.tuwien.dsg.hadl.framework.runtime.events.SensorFactory;
import at.ac.tuwien.dsg.hadl.framework.runtime.impl.*;
import at.ac.tuwien.dsg.hadl.framework.runtime.intf.FactoryResolvingException;
import at.ac.tuwien.dsg.hadl.schema.core.HADLmodel;
import com.google.inject.*;
import fj.data.Either;
import fj.data.Option;
import net.laaber.mt.dsl.lib.hadl.*;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import net.laaber.mt.hadl.atlassian.rest.BitbucketClient;
import net.laaber.mt.hadl.atlassian.rest.HipChatClient;
import net.laaber.mt.hadl.atlassian.sensor.AtlassianSensorFactory;
import org.junit.*;
import org.junit.rules.TestName;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.test.TestGraphDatabaseFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.junit.Assert.assertTrue;

/**
 * Created by Christoph on 28.10.15.
 */
public abstract class BaseTest {
    private final static String MODEL_FILE = "./src/test/resources/input/agile-hADL.xml";
    private final static String OUTPUT_PATH_PREFIX = "src/test/resources/output/";

    protected static Properties properties;

    protected static HipChatClient hcClient;
    protected static BitbucketClient bbClient;

    @BeforeClass
    public static void _setUpClass() throws IOException {
        properties = new Properties();
        properties.loadFromXML(BaseTest.class.getClassLoader().getResourceAsStream("atlassianCredentials.xml"));
        Assert.assertFalse("Could not load properties", properties.isEmpty());
        hcClient = AtlassianClientFactory.instance().hipChatClient();
        bbClient = AtlassianClientFactory.instance().bitbucketClient();
    }

    protected ProcessScope ps;

    @Rule
    public TestName testName = new TestName();

    @Before
    public void _setUp() {
        final HadlModelInteractor mi = new HadlModelInteractorImpl();
        Either<Throwable, HADLmodel> loadResult = mi.load(MODEL_FILE);
        assertTrue(loadResult.isRight());
        final HADLmodel model = loadResult.right().value();

        final Injector baseInjector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
            }

            @Provides
            @Singleton
            Executable2OperationalTransformer getTransformer() {
                return new Executable2OperationalTransformer();
            }

            @Provides
            @Singleton
            ModelTypesUtil getTypesUtil() {
                ModelTypesUtil mtu = new ModelTypesUtil();
                mtu.init(model);
                return mtu;
            }
        });

        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(HADLLinkageConnector.class).in(Singleton.class);
                bind(HADLruntimeMonitor.class).in(Singleton.class);
                bind(HadlLinker.class).to(HadlLinkerImpl.class).in(Singleton.class);
                bind(HadlMonitor.class).to(HadlMonitorImpl.class).in(Singleton.class);
                bind(ProcessScope.class).in(Singleton.class);
                bind(SensorFactory.class).to(AtlassianSensorFactory.class).in(Singleton.class);
            }

            @Provides
            @Singleton
            HadlModelInteractor getModelInteractor() {
                return mi;
            }

            @Provides
            @Singleton
            Executable2OperationalTransformer getTransformer() {
                return baseInjector.getInstance(Executable2OperationalTransformer.class);
            }

            @Provides
            @Singleton
            RuntimeRegistry getRegistry() {
                return new RuntimeRegistry();
            }

            @Provides
            @Singleton
            ModelTypesUtil getTypesUtil() {
                return baseInjector.getInstance(ModelTypesUtil.class);
            }

            @Provides
            @Singleton
            SurrogateFactoryResolver getFactoryResolver() {
                SurrogateFactoryResolver sfr = new SurrogateFactoryResolver();
                try {
                    sfr.resolveFactoriesFrom(model);
                } catch (FactoryResolvingException e) {
                    e.printStackTrace();
                }
                return sfr;
            }

            @Provides
            @Singleton
            HADLruntimeModel getRuntimeModel() {
                Neo4JbackedRuntimeModel hrm = new Neo4JbackedRuntimeModel();
                baseInjector.injectMembers(hrm);
                // setup Neo4J DB
                GraphDatabaseService graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
                hrm.init(graphDb, model);
                return hrm;
            }
        });
        ps = injector.getInstance(ProcessScope.class);

        initLinker();
    }


    @Before
    public void printBeginDelimiter() {
        System.out.println("---------- " + testName.getMethodName() + " BEGIN");
    }

    @After
    public void printAfterDelimiter() {
        System.out.println("---------- " + testName.getMethodName() + " END");
    }

    @After
    public void _tearDown() {
        storeRt(testName.getMethodName() + ".xml");
        ps.getLinkageConnector().shutdown();
        ps = null;
    }

    private void initLinker() {
        Option<Throwable> setUpError = ps.getLinker().setUp(null);
        assertTrue(setUpError.isNone());
    }

    private void storeRt(String fileName) {
        List<String> e = new ArrayList<>();
        e.add("at.ac.tuwien.dsg.hadl.schema.extension.google");
        Option<Throwable> r = ps.getModelInteractor().storeRuntime(ps.getModel(), ps.getRuntimeModel(), e, OUTPUT_PATH_PREFIX, fileName);
        if (r.isSome()) {
            r.some().printStackTrace();
        }
    }

    protected void handleError(Either<Throwable, ?> res) {
        if (res.isLeft()) {
            Throwable t = res.left().value();
            t.printStackTrace();
            Assert.fail(t.getMessage());
        }
    }

    protected void stopScope() {
        final Option<Throwable> stopResult = ps.getLinker().stopScope();
        if (stopResult.isSome()) {
            Assert.fail(stopResult.some().getMessage());
        }
    }

    protected void startScope() {
        final Option<Throwable> startResult = ps.getLinker().startScope();
        if (startResult.isSome()) {
            Assert.fail(startResult.some().getMessage());
        }
    }
}
