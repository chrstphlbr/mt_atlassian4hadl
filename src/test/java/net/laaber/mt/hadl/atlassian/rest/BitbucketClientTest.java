package net.laaber.mt.hadl.atlassian.rest;

import fj.data.Option;
import net.laaber.mt.hadl.atlassian.BaseTest;
import net.laaber.mt.hadl.atlassian.BitbucketConstants;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Repository;
import net.laaber.mt.hadl.atlassian.data.bitbucket.User;
import net.laaber.mt.hadl.atlassian.data.bitbucket.Wiki;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by Christoph on 27.10.15.
 */
public class BitbucketClientTest extends BaseTest {

    private static BitbucketClient client;
    private static String teamName;
    private static String repository;

    @BeforeClass
    public static void setUpClass() throws IOException {
        String adminLogin = properties.getProperty(BitbucketConstants.AdminLogin);
        Assert.assertNotNull("adminLogin is null", adminLogin);
        String adminPassword = properties.getProperty(BitbucketConstants.AdminPassword);
        Assert.assertNotNull("adminPassword is null", adminPassword);
        String apiKey = properties.getProperty(BitbucketConstants.ApiKey);
        Assert.assertNotNull("apiKey is null", apiKey);
        String apiSecret = properties.getProperty(BitbucketConstants.ApiSecret);
        Assert.assertNotNull("apiSecret is null", apiSecret);

        client = new BitbucketClientImpl(adminLogin, adminPassword, apiKey, apiSecret);

        teamName = properties.getProperty(BitbucketConstants.Teamname);
        Assert.assertNotNull("teamname is null", teamName);
        repository = properties.getProperty(BitbucketConstants.Repository);
        Assert.assertNotNull("repository is null", repository);

        // create TestPage
        client.createWiki(teamName, repository, "TestPage", "This is my test page!");
    }

    @AfterClass
    public static void tearDownClass() {
        System.out.println("delete 'TestPage' and 'TestPage1' wiki page");
    }

    @Test
    public void user() {
        Option<User> user = client.user("chrstphlbr");
        Assert.assertFalse("no user returned", user.isNone());
        User u = user.some();
        Assert.assertEquals("wrong email", "chrstphlbr", u.getUsername());
    }

    @Test
    public void repository() {
        Option<Repository> repo = client.repository(teamName, repository);
        Assert.assertFalse("No Repository returned", repo.isNone());
        Assert.assertTrue("MT_Evaluation".equals(repo.some().getName()));
    }

    @Test
    public void repositories() {
        Option<List<Repository>> repos = client.repositories(teamName);
        Assert.assertFalse("No repositories returned", repos.isNone());
        List<Repository> rs = repos.some();
        Assert.assertTrue(rs.size() == 1);
        Repository r = rs.get(0);
        Assert.assertTrue("MT_Evaluation".equals(r.getName()));
    }

    @Test
    public void teamMembers() {
        Option<List<User>> optUsers = client.teamMembers(teamName);
        Assert.assertFalse(optUsers.isNone());
        List<User> teamMembers = optUsers.some();
        Assert.assertEquals("incorrect teamMembers size", 4, teamMembers.size());
        Assert.assertTrue("chrstphlbr".equals(teamMembers.get(0).getUsername()));
    }

    @Test
    public void wiki() {
        final String pageTitle = "TestPage";
        final String content = "This is my test page!";
        Option<Wiki> optWiki = client.wiki(teamName, repository, pageTitle);
        Assert.assertFalse("no wiki returned", optWiki.isNone());
        Wiki w = optWiki.some();
        Assert.assertTrue("wrong page: expected '" + pageTitle + "', was '" + w.getPage() + "'", pageTitle.equals(w.getPage()));
        Assert.assertTrue("wrong data: expected '" + content + "' was '" + w.getData() + "'", content.equals(w.getData()));
    }

    @Test
    public void createWiki() {
        final String pageTitle = "TestPage1";
        final String content = "TestPage1 content";
        boolean success = client.createWiki(teamName, repository, pageTitle, content);
        Assert.assertTrue("createWiki did not return success", success);

        Option<Wiki> optWiki = client.wiki(teamName, repository, pageTitle);
        Assert.assertFalse("no wiki returned", optWiki.isNone());
        Wiki w = optWiki.some();
        Assert.assertTrue("wrong page: expected '" + pageTitle + "', was '" + w.getPage() + "'", pageTitle.equals(w.getPage()));
        Assert.assertTrue("wrong data: expected '" + content + "' was '" + w.getData() + "'", content.equals(w.getData()));
    }

//    @Test
//Test does not work because Bitbucket API is not working properly
    public void updateWiki() {
        final String pageTitle = "TestPage";
        final String content = "Edited TestPage!";
        final String path = "TestPage";
        boolean success = client.updateWiki(teamName, repository, pageTitle, path, content);
        Assert.assertTrue("updateWiki did not return success", success);
        Option<Wiki> optWiki = client.wiki(teamName, repository, pageTitle);
        Assert.assertFalse("no wiki returned", optWiki.isNone());
        Wiki w = optWiki.some();
        Assert.assertTrue("wrong page: expected '" + pageTitle + "', was '" + w.getPage() + "'", pageTitle.equals(w.getPage()));
        Assert.assertTrue("wrong data: expected '" + content + "' was '" + w.getData() + "'", content.equals(w.getData()));
    }

}
