package net.laaber.mt.hadl.atlassian.surrogate;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.core.THumanComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import fj.data.Either;
import net.laaber.mt.hadl.atlassian.BaseTest;
import net.laaber.mt.hadl.atlassian.HadlConstants;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TAtlassianResourceDescriptor;
import net.laaber.mt.hadl.atlassian.rest.AtlassianClientFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Christoph on 12.11.15.
 */
public class UserSurrogateTest extends BaseTest {

    @Test
    public void acquire() {
        String userEmail = "hadltestuser1@gmail.com";

        final Either<Throwable, THADLarchElement> userResult = ps.getLinker().acquire(HadlConstants.Col.DevUser, AtlassianResourceDescriptorFactory.atlassian("User1", userEmail, userEmail));
        handleError(userResult);
        TOperationalComponent user = (TOperationalComponent) userResult.right().value();
        Assert.assertNotNull("returned user is null", user);
        THumanComponent type = (THumanComponent) ps.getModelTypesUtil().getByTypeRef(user.getInstanceOf());
        Assert.assertEquals("incorrect hADL type", HadlConstants.Col.DevUser, type.getId());
        TAtlassianResourceDescriptor rd = (TAtlassianResourceDescriptor) user.getResourceDescriptor().get(0);
        Assert.assertEquals("incorrect bitbucket rd", userEmail, rd.getBitbucket().getBitbucketId());
        Assert.assertEquals("incorrect hipchat rd", userEmail, rd.getHipChat().getHipChatId());
        Assert.assertEquals("operational in incorrect state", TOperationalState.DESCRIBED_EXISTING, user.getState());
    }
}
