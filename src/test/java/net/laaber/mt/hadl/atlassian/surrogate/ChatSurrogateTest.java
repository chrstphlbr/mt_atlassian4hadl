package net.laaber.mt.hadl.atlassian.surrogate;

import at.ac.tuwien.dsg.hadl.schema.core.THADLarchElement;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalCollabLink;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalComponent;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalObject;
import at.ac.tuwien.dsg.hadl.schema.runtime.TOperationalState;
import fj.data.Either;
import fj.data.Option;
import net.laaber.mt.hadl.atlassian.BaseTest;
import net.laaber.mt.hadl.atlassian.HadlConstants;
import net.laaber.mt.hadl.atlassian.data.hipChat.Chat;
import net.laaber.mt.hadl.atlassian.data.hipChat.User;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.AtlassianResourceDescriptorFactory;
import net.laaber.mt.hadl.atlassian.resourceDescriptor.TAtlassianResourceDescriptor;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christoph on 10.11.15.
 */
public class ChatSurrogateTest extends BaseTest {

    @Test
    public void acquireNotExisting() {
        String rid = "TestChat-acquireNonExisting";
        // acquire non existing chat
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Chat, AtlassianResourceDescriptorFactory.hipChat(rid, rid));
        handleError(acquireResult);
        TOperationalObject o = (TOperationalObject) acquireResult.right().value();
        Assert.assertNotNull("no operational returned", o);

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        final Option<Chat> chat = hcClient.chat(rid);
        Assert.assertTrue("no chat returned", chat.isSome());
        Chat c = chat.some();
        Assert.assertEquals("Incorrect chat returned", rid, c.getName());
        Assert.assertEquals("operational in incorrect state", TOperationalState.DESCRIBED_EXISTING, o.getState());

        // delete room
        hcClient.deleteChat(rid);
    }

    @Test
    public void acquireExisting() {
        String rid = "TestChat-acquireExisting";

        // create chat
        final Option<String> createResult = hcClient.createChat(rid);
        if (createResult.isNone()) {
            Assert.fail("could not create chat");
        }

        // acquire existing chat
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Chat, AtlassianResourceDescriptorFactory.hipChat(rid, rid));
        handleError(acquireResult);
        TOperationalObject o = (TOperationalObject) acquireResult.right().value();
        Assert.assertNotNull("no operational returned", o);
        Assert.assertEquals("operational in incorrect state", TOperationalState.DESCRIBED_EXISTING, o.getState());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        final Option<Chat> chat = hcClient.chat(rid);
        Assert.assertTrue("no chat returned", chat.isSome());
        Chat c = chat.some();
        Assert.assertEquals("Incorrect name", rid, c.getName());
        Assert.assertEquals("Incorrect id", createResult.some(), String.valueOf(c.getId()));

        // delete room
        hcClient.deleteChat(rid);
    }

    @Test
    public void release() {
        String rid = "TestChat-release";
        final Either<Throwable, THADLarchElement> acquire = ps.getLinker().acquire(HadlConstants.Obj.Chat, AtlassianResourceDescriptorFactory.hipChat(rid, rid));
        handleError(acquire);
        TOperationalObject chat = (TOperationalObject) acquire.right().value();
        Assert.assertNotNull("no operational returned", chat);
        final Option<Throwable> release = ps.getLinker().release(chat);
        if (release.isSome()) {
            Throwable r = release.some();
            Assert.fail(r.getMessage());
        }
        Assert.assertEquals("operational in incorrect state", TOperationalState.DESCRIBED_NONEXISTING, chat.getState());
        final Option<Chat> chatResult = hcClient.chat(rid);
        Assert.assertTrue("chat returned", chatResult.isNone());
    }

    @Test
    public void link() {
        String id = "TestChat-link";
        invite(id);
        hcClient.deleteChat(id);
    }

    private List<TOperationalCollabLink> invite(String roomName) {
        // chat
        final Either<Throwable, THADLarchElement> acquireResult = ps.getLinker().acquire(HadlConstants.Obj.Chat, AtlassianResourceDescriptorFactory.hipChat(roomName, roomName));
        handleError(acquireResult);
        TOperationalObject chat = (TOperationalObject) acquireResult.right().value();
        Assert.assertNotNull("no operational returned for chat", chat);
        // chat participants
        String user1Mail = "hadltestuser1@gmail.com";
        final Either<Throwable, THADLarchElement> user1Result = ps.getLinker().acquire(HadlConstants.Col.DevUser, AtlassianResourceDescriptorFactory.atlassian("User1", user1Mail, user1Mail));
        handleError(user1Result);
        TOperationalComponent user1 = (TOperationalComponent) user1Result.right().value();
        Assert.assertNotNull("no operational returned for user1", user1);
        Assert.assertEquals("incorrect bb rd for user1", user1Mail, ((TAtlassianResourceDescriptor) user1.getResourceDescriptor().get(0)).getBitbucket().getBitbucketId());
        Assert.assertEquals("incorrect hc rd for user1", user1Mail, ((TAtlassianResourceDescriptor) user1.getResourceDescriptor().get(0)).getHipChat().getHipChatId());
        Option<User> u1Res = hcClient.user(user1Mail);
        Assert.assertTrue("no user 1 returned", u1Res.isSome());
        User u1 = u1Res.some();

        String user2Mail = "hadltestuser2@gmail.com";
        final Either<Throwable, THADLarchElement> user2Result = ps.getLinker().acquire(HadlConstants.Col.DevUser, AtlassianResourceDescriptorFactory.atlassian("User2", user2Mail, user2Mail));
        handleError(user2Result);
        TOperationalComponent user2 = (TOperationalComponent) user2Result.right().value();
        Assert.assertNotNull("no operational returned for user2", user2);
        Assert.assertEquals("incorrect bb rd for user2", user2Mail, ((TAtlassianResourceDescriptor) user2.getResourceDescriptor().get(0)).getBitbucket().getBitbucketId());
        Assert.assertEquals("incorrect hc rd for user2", user2Mail, ((TAtlassianResourceDescriptor) user2.getResourceDescriptor().get(0)).getHipChat().getHipChatId());
        Option<User> u2Res = hcClient.user(user2Mail);
        Assert.assertTrue("no user 2 returned", u2Res.isSome());
        User u2 = u2Res.some();

        // chat invites
        stopScope();

        final Either<Throwable, TOperationalCollabLink> user1InviteResult = ps.getLinker().link(user1, chat, HadlConstants.Link.InviteChat);
        handleError(user1InviteResult);
        TOperationalCollabLink user1Invite = user1InviteResult.right().value();
        Assert.assertNotNull("no operational returned for user1Invite", user1Invite);
        Assert.assertEquals("user1Invite in incorrect state", TOperationalState.PRESCRIBED_EXISTING, user1Invite.getState());

        final Either<Throwable, TOperationalCollabLink> user2InviteResult = ps.getLinker().link(user2, chat, HadlConstants.Link.InviteChat);
        handleError(user2InviteResult);
        TOperationalCollabLink user2Invite = user2InviteResult.right().value();
        Assert.assertNotNull("no operational returned for user2Invite", user2Invite);
        Assert.assertEquals("user2Invite in incorrect state", TOperationalState.PRESCRIBED_EXISTING, user2Invite.getState());


        startScope();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        // check invites
        final Option<List<User>> invitedUserResult = hcClient.usersInChat(roomName);
        if (invitedUserResult.isNone()) {
            Assert.fail("no user list returned for " + roomName);
        }
        List<User> invitedUsers = invitedUserResult.some();
        Assert.assertEquals("wrong size of invited users", 3, invitedUsers.size());
        Assert.assertTrue("user 1 not invited", invitedUsers.contains(u1));
        Assert.assertTrue("user 2 not invited", invitedUsers.contains(u2));

        // link operational states
        Assert.assertEquals("user1Invite in incorrect state", TOperationalState.DESCRIBED_EXISTING, user1Invite.getState());
        Assert.assertEquals("user2Invite in incorrect state", TOperationalState.DESCRIBED_EXISTING, user2Invite.getState());

        List<TOperationalCollabLink> links = new ArrayList<>();
        links.add(user1Invite);
        links.add(user2Invite);
        return links;
    }

    @Test
    public void unlink() {
        String roomName = "TestChat-unlink";
        List<TOperationalCollabLink> links = invite(roomName);

        // uninvite
        stopScope();

        for (TOperationalCollabLink l : links) {
            final Option<Throwable> unlinkResult = ps.getLinker().unlink(l);
            if (unlinkResult.isSome()) {
                unlinkResult.some().printStackTrace();
                Assert.fail(unlinkResult.some().getMessage());
            }
            Assert.assertEquals("link in incorrect state", TOperationalState.PRESCRIBED_NONEXISTING, l.getState());
        }

        startScope();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        final Option<List<User>> invitedUsersResult = hcClient.usersInChat(roomName);
        if (invitedUsersResult.isNone()) {
            Assert.fail("no invited users returned");
        }

        List<User> invitedUsers = invitedUsersResult.some();
        Assert.assertEquals("incorrect number of invited users", 1, invitedUsers.size());

        // check operational states
        for (TOperationalCollabLink l : links) {
            Assert.assertEquals("link in incorrect state", TOperationalState.DESCRIBED_NONEXISTING, l.getState());
        }

        hcClient.deleteChat(roomName);
    }

}
