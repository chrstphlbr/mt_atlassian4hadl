# hADL Surrogates and Sensors for [Bitbucket](https://bitbucket.org/) and [HipChat](https://hipchat.com/)

This repository offers basic REST-clients, Surrogates and Sensors for Bitbucket and HipChat for [hADL](https://bitbucket.org/christophdorn/hadl). It is used in the Evaluation of Christoph Laaber's [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis).

## Required Projects
The following dependancies are not available through a public Maven repository. Hence one must install them to the local Maven repository before installing.
 
 * [hADL Schema and Runtime](https://bitbucket.org/christophdorn/hadl)
 * [hADL Synchronous Library](https://bitbucket.org/chrstphlbr/mt_dsl-lib)

## Installation
For installing easily repress executing the unit tests.
```
mvn install -Dmaven.test.skip=true
```

For properly installing the project, have a look at Christoph Laaber's [master thesis](https://bitbucket.org/chrstphlbr/mt_thesis) especially section 5.1.5.